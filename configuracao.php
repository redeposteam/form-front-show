<?php
/**
 * Created by PhpStorm.
 * User: thomassoares
 * Date: 2019-04-05
 * Time: 14:32
 */

class configuracao
{


    public $configuracao =  array(
        "pernambuco"=>array(
            'url_logo' => 'images/logos/pedasorte.png',
            'url_logo_footer' => 'images/logos/pedasorte-shadow.png',
            'telefone' => '(81) 3081-7999',
            'faleconosco' => 'faleconosco@pernambucodasorte.com.br',
            'endereco' => 'Pernambuco dá Sorte - End: Avenida Caxangá, 3455, Iputinga, Recife - PE <br/>CEP: 50.670-000',
            'telegram' => '(81) 98181-6885',
            'instagram' => 'pedasorte',
            'facebook' => 'pernambucodasorte',
            'link_adquira' => 'https://doecap.com.br/vendainternet/comprar/certificado/pernambuco',
            'nome' => 'Pernambuco dá Sorte',
            'twitter' => 'capixaba',
            'favicon' => 'favicon_pernambuco.ico'
        ),
        "feira"=>array(
            'url_logo' => 'images/logos/feiradasorte.png',
            'url_logo_footer' => 'images/logos/feiradasorte-shadow.png',
            'telefone' => '(75) 3225-7005',
            'faleconosco' => 'faleconosco@feiradasorte.com.br',
            'endereco' => 'Feira Dá Sorte - End: RUA BARÃO DO RIO BRANCO, Nº 1290, LOJA 08, FEIRA DE SANTANA - BA <br/>CEP: 44.001-232',
            'telegram' => '',
            'instagram' => 'feiradasorte',
            'facebook' => 'feiradasorte',
            'link_adquira' => 'https://doecap.com.br/vendainternet/comprar/certificado/feira',
            'nome' => 'Feira dá Sorte',
            'twitter' => 'capixaba',
            'favicon' => 'favicon_feiras.ico'
        ),
        "goias"=>array(
            'url_logo' => 'images/logos/godasorte.png',
            'url_logo_footer' => 'images/logos/godasorte-shadow.png',
            'telefone' => '(62) 3520-9800',
            'faleconosco' => 'faleconosco@goiasdasorte.com.br',
            'endereco' => 'Goiás dá Sorte - End: Rua 210, nº 326, Setor Coimbra, Goiânia - GO<br/>CEP: 74.535-280',
            'telegram' => '',
            'instagram' => 'goiasdasorte',
            'facebook' => 'godasorte',
            'link_adquira' => 'https://doecap.com.br/vendainternet/comprar/certificado/goias',
            'nome' => 'Goiás dá Sorte',
            'twitter' => 'capixaba',
            'favicon' => 'favicon_goias.ico'
        ),
        "capixaba"=>array(
            'url_logo' => 'images/logos/capixabacap.png',
            'url_logo_footer' => 'images/logos/capixabacap-shadow.png',
            'telefone' => '(27) 3026-3338',
            'faleconosco' => 'faleconosco@capixabacap.com.br',
            'endereco' => 'Capixaba Cap - End: Avenida Vitória, nº 3128, Bento Ferreira, Vitória - ES, <br/>CEP: 29.050-800',
            'telegram' => '',
            'instagram' => 'capixabacap',
            'facebook' => 'CapixabaCap',
            'link_adquira' => 'https://doecap.com.br/vendainternet/comprar/certificado/capixaba',
            'nome' => 'Capixaba Cap',
            'twitter' => 'capixaba',
            'favicon' => 'favicon_capixaba.ico'
        ),
        "carimbo"=>array(
            'url_logo' => 'images/logos/carimbodasorte.png',
            'url_logo_footer' => 'images/logos/carimbodasorte-shadow.png',
            'telefone' => '(91) 3250-5239',
            'faleconosco' => 'faleconosco@carimbodasorte.com.br',
            'endereco' => 'Carimbó dá Sorte - End: AV. Almirante Tamandaré, nº 948, Campina - Belém - PA <br/>CEP: 66.023-000',
            'telegram' => '',
            'instagram' => 'carimbodasorte',
            'facebook' => 'cadasorte',
            'link_adquira' => 'https://doecap.com.br/vendainternet/comprar/certificado/carimbo',
            'nome' => 'Carimbó dá Sorte',
            'twitter' => 'capixaba',
            'favicon' => 'favicon_carimbo.ico'
        ),
        "alagoas"=>array(
            'url_logo' => 'images/logos/aldasorte.png',
            'url_logo_footer' => 'images/logos/aldasorte-shadow.png',
            'telefone' => '(82) 3221-0421',
            'faleconosco' => 'faleconosco@alagoasdasorte.com.br',
            'endereco' => 'Alagoas dá Sorte - End: Rua do Sol, nº 451, Centro - Maceió - AL <br/>CEP: 57.020-070',
            'telegram' => '',
            'instagram' => 'alagoasdasorte',
            'facebook' => 'aldasorte',
            'link_adquira' => 'https://doecap.com.br/vendainternet/comprar/certificado/alagoas',
            'nome' => 'Alagoas dá Sorte',
            'twitter' => 'capixaba',
            'favicon' => 'favicon_alagoas.ico'
        ),
        "amazonas"=>array(
            'url_logo' => 'images/logos/amdasorte.png',
            'url_logo_footer' => 'images/logos/amdasorte-shadow.png',
            'telefone' => '(92) 3621-4100',
            'faleconosco' => 'faleconosco@amazonasdasorte.com.br',
            'endereco' => 'Amazonas dá Sorte - End: Av. Tarumã, 1156, Praça 14 de Janeiro, Manaus - AM <br/>CEP: 69.020-000',
            'telegram' => '',
            'instagram' => 'amazonasdasorte',
            'facebook' => 'amdasorte',
            'link_adquira' => 'https://doecap.com.br/vendainternet/comprar/certificado/amazonas',
            'nome' => 'Amazonas dá Sorte',
            'twitter' => 'capixaba',
            'favicon' => 'favicon_amazonas.ico'
        ),
        "bahia"=>array(
            'url_logo' => 'images/logos/badasorte.png',
            'url_logo_footer' => 'images/logos/badasorte-shadow.png',
            'telefone' => '(71) 3245-1678',
            'faleconosco' => 'faleconosco@bahiadasorte.com.br',
            'endereco' => 'Bahia dá Sorte - End: Av. Vasco Da Gama, nº 215 Federação- Salvador - BA <br/>CEP: 40.230-731',
            'telegram' => '(71) 98149-5074',
            'instagram' => 'bahiadasorte',
            'facebook' => 'bahiadasorte',
            'link_adquira' => 'https://doecap.com.br/vendainternet/comprar/certificado/bahia',
            'nome' => 'Bahia dá Sorte',
            'twitter' => 'capixaba',
            'favicon' => 'favicon_bahia.ico'
        )
    );


}
