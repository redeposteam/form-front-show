// const baseUrlAixmobil = "https://pagsorte.showdasorte.com/";
const baseUrlAixmobil = "http://showdasorte-stage.redepos.com.br/aixmobil/";
// const baseUrlAixmobil = "http://mobile-stage.redepos.com.br/pagsorte/";
// const baseUrlAixmobil = "/";
// const baseUrlAixmobil = "http://127.0.0.1:8089/";
// const baseUrlPagseguro = "https://pagseguro.showdasorte.com/";
const baseUrlPagseguro = "http://showdasorte-stage.redepos.com.br/pagseguro/";
// const baseUrlPagseguro = "http://mobile-stage.redepos.com.br/pagseguro/";
// const baseUrlPagseguro = "/";
// const baseUrlPagseguro = "http://127.0.0.1:8089/";
// const baseUrlPicpay = "http://mobile-stage.redepos.com.br/picpay/";
// const baseUrlPicpay = "https://picpay.showdasorte.com/";
const baseUrlPicpay = "http://showdasorte-stage.redepos.com.br/picpay/";
// const baseUrlPicpay = "/";
// const baseUrlJuno = "http://mobile-stage.redepos.com.br/juno/";
// const baseUrlJuno = "http://127.0.0.1:8092/";
// const baseUrlJuno = "https://juno.showdasorte.com/";
const baseUrlJuno = "http://showdasorte-stage.redepos.com.br/juno/";
// const baseUrlJuno = "/";

var prod = false;
var getArquivo            = baseUrlAixmobil+'aixmobil/get/ler';
var postCheckout          = baseUrlAixmobil+'aixmobil/post/checkout';
var postCheckoutPagSeguro = baseUrlPagseguro+'pagseguro/post/checkout';
var postCheckoutJuno      = baseUrlJuno+'juno/post/checkout';
var postCheckoutToken     = baseUrlAixmobil+'aixmobil/post/checkout-token';
var postremoverCartao     = baseUrlAixmobil+'aixmobil/post/remover-cartao';

// mascaras dos campos
$("#cpf").mask('000.000.000-00');
$("#telefone").mask('(00) 00000-0000');
$("#telefoneCadastro").mask('(00) 00000-0000');
$("#telefoneRecado").mask('(00) 00000-0000');
$("#cep").mask('00000-000');
// $('#numero_cartao').mask('0000.0000.0000.0000');
$('#codigo').mask('0000');
$('#validade').mask('00/0000', {clearIfNotMatch: true});

$("#agencia").mask('0000');
$("#conta").mask('0000000000');
$("#numeroResidencia").mask('0000000');
$("#identidade").mask('0000000000000');


function errorPage(msg){
    localStorage.setItem('errorMsg', msg);
    window.location = 'error.html';
}

function alterarMetodoPagamento(e){
    const valor = $(e).val();
    console.log(valor);
    if(valor === '0'){
        $('.general-save-card').hide();
    }else{
        $('.general-save-card').show();
    }
}
function mostrarFormulario() {
    $(".card-selector").hide();
    $("#myForm").show();
    $('#numero_cartao').click().focusout();
}
function mostrarCartoesCadastrados() {
    $("#myForm").hide();
    $(".card-selector").show();
    $(".request-card").show();
}
function validaCamposVazios(elmErr){
    var campos = [];

    $(".form-control").each(function(e){
        if(!$(this).val() && $(this).attr('required')){
            campos.push($(this).data('campo'));
        }
    });

    return campos;
}
function valida_campos() {
// realiza a validação ao passar para o próximo step
    $("#smartwizard").on("leaveStep", function (e, anchorObject, stepNumber, stepDirection) {
        var elmForm = $("#form-step-" + stepNumber);

        if (stepDirection === 'forward' && elmForm) {
            elmForm.validator('validate');
            var inputComum = elmForm.find('.form-group .form-control');

            if (inputComum.hasClass('is-invalid')) {
                inputComum.removeClass('is-invalid');
            }
            var elmErr = elmForm.children('.has-error');
            var input = elmErr.children('.form-control');
            input.addClass('is-invalid');

            if (elmErr && elmErr.length > 0) {
                return false;
            }
        }

        return true;
    });
}
function valida_bandeira(e){
    var bandeira = $(e).attr('bandeira');

    $('#flag_card').val(bandeira);
    if($(e).val().length > 12) {
        switch (bandeira) {
            case 'visa':
                $('#bandeira').val('1');
                break;
            case 'mastercard':
                $('#bandeira').val('2');
                break;
            case 'elo':
                $('#bandeira').val('3');
                break;
            case 'diners':
                $('#bandeira').val('4');
                break;
            case 'unknown':
                swetalert('info', 'Pagamento disponível para cartões: <br> <b>Mastercard</b>, <b>Visa</b>, <b>Elo</b> e <b>Diners</b>');
                $('#bandeira').val();
                break
        }
    }
}
function onlynumber(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    //var regex = /^[0-9.,]+$/;
    var regex = /^[0-9.]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}
$("#nome_titular").blur(function () {
    var nome = $(this).val();
    $(this).val(nome.toUpperCase());
});

function mostrarAjudaCVV() {
    $('#modal-ajuda-cvv').modal('show');
}

function mostrarAjudaCVVJuno() {
    $('#modal-ajuda-cvv-juno').modal('show');
}

