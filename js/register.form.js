//AIXMOBIL
var varTokenPublico = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDWmPSTgzpQoNBoz4K0OAKPbPzDmkDe9X31nrFaDN5S9BhOmMefbJ5HQx30TrBYWMHDWsGpCYUH8o48L4EDoPRTHXh9BHCGdPIDwO8D0mOdDVjJG5/LC+cZ9UMx30NziweEe04l7Bs/t1H2hapgIDIDYe22N4eAvBaEYWzsdFxJRwIDAQAB';
var varHostname = 'https://dev.aixmobil.com/rest/';
// var varTokenPublico = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC7vKJL0vX2v/9gORTYeHAqLFXVWOSDuU9EvtxFoCw1bCH/rrgPpSl0CgeUrOxNSyNyW03uEYJAaqa9U35RPHuxFSBNbp8ZY8xr7wavL3x6P+pg4LtolPXEvqNZVaMPd9z7r8+48S/SMhnx54E29eXvjoMOty7ZtzpsEOTUAInUBwIDAQAB';
// var varHostname = 'https://payment.aixmobil.com/rest/';
var tokenAixmobil;

//JUNO
var junoToken = '7E18535FD586D7BD225049017DB48A13818A2EF1E898CDCF947BE13A98656B4A'; // DEV
// var junoToken = '13209EE955868CBE2F8EEDAB43D6C85FFB0FDBFC66E10D99DF46EA1C1186DDBD6C1E7E81BFA40290';
// const varAmbienteProducao = true;
const varAmbienteProducao = false;
let validatorJuno;
// ao executar página
$(document).ready(function () {

    validatorJuno = validarFormularioJuno();
    // verifica se foi informado um hash de cliente/usuario

    //localStorage.setItem('hash', '{"qtd":2,"canalVenda":1,"id_transacao":0,"nome":"Thomás Soare","telefone":"(81) 98558484","email":"soares.thom@gmail.com","cpf":"09190874420","nascimento":"07/11/1990","logradouro":"","numero":"","bairro":"","cep":"","cidade":"","estado":"","complemento":"","numCard":"4111111111111111","mesValidadeCard":"12","anoValidadeCard":"2030","codSegCard":"123","bandCard":"","parcelas":1,"remote":"1.1.1.1","idVendaConsolidada":[],"prodId":"2218","prodDescricao":"R$ 10,00","tipo":0,"valor":20,"id":"5610061","idEmpresa":"69","idEdicao":"7727","idTipoLoteria":"450"}');

    var params = window.location.search;
    //alert(params);
    if (prod !== true || params !== "") {
        //const urlParams = new URLSearchParams(window.location.search);
        const hash = JSON.parse(localStorage.getItem('hash'));

        //console.log(localParams);
        if (hash === null) {
            $('body').html('<h1 style="color: red; text-align: center">Atenção</h1>' +
                '<h3 style="text-align: center">Não foi possível prosseguir com a solicitação. Verifique em seu e-mail se seu cadastro foi ativado e tente novamente.</h3>');
            $('#loading').fadeOut('slow');
            return;
        } else {

            //const urlParams = new URLSearchParams(window.location.search);
            let base = '';
            //const hasBase = localParams;
            $.get('https://config.showdasorte.com/config.php', function (response) {

                const data = JSON.parse(response);

                // const lista_gatways_on_page = [];
                // $('.gateway').each(function () {
                //     lista_gatways_on_page.push($(this).data('gateway'));
                // });
                mostrarGateways(data.payments);


                // mostrarFluxo(data.gateway.toString().toLowerCase());
                mostrarFluxo('pagsorte');
                // const gateway = urlParams.get('gateway').toString().toLowerCase();
                // mostrarFluxo(gateway);

                buscaDadosHash();
            });


        }
    } else {
        //window.location.href = baseUrl + "/pagamento";
    }

    var tipo_cartao = $('input[name="forma_pagamento"]:checked').val();
    $('.btn-save-card').on('click', function () {
        $('#checkbox-content i').toggleClass('active');
    });
    $('#checkbox-content i').on('click', function () {
        $('.btn-save-card').click();
    });
});

function setCsrfStorage(response) {

    if (response.CsrfToken !== undefined && response.CsrfToken !== '') {
        localStorage.setItem('dtcsrf', response.CsrfToken);
    }
}

function buscaDadosHash() {
    // var params = window.location.search;
    // params = params.split('hash=');
    // localStorage.hash = localStorage.getItem('hash');
    // const urlParams = new URLSearchParams(window.location.search);
    let response;

    if (localStorage.getItem('hash') !== null && localStorage.getItem('hash') !== '') {
        response = JSON.parse(localStorage.getItem('hash'));
    } else {

        swetalert('erro', 'Não foi possível buscar os dados do cliente');
    }

    mostrarDadosDaCompra(response);
    if (response === null) {
        const msg = "<h1 style='text-align: center'>Atenção! Por questões de segurança não foi possível prosseguir com a compra. Por favor tente novamente!</h1>";
        $('.contentAll').html(msg);
        return;
    }


    // if (response !== null) {
    var contentValor = parseFloat(response.valor).toFixed(2);
    contentValor = 'R$ ' + contentValor.toString().replace('.', ',');
    $('#dados-valor').html(contentValor);
    $('#dados-valor-juno').html(contentValor);
    $('#valor').show();
    $('#valor-juno').show();

    preencheDadosCliente(JSON.stringify(response)).then(() => {

        // if (response.data.cartoes_html !== undefined) {
        //     var quantidade_cartoes = response.data.cartoes_html.length;
        //     var cartoes;
        //     if (quantidade_cartoes > 0) {
        //         $('.btn-valida-lista-cartoes').show();
        //         for (i = 0; i < quantidade_cartoes; i++) {
        //             $('#cards-list').append(response.data.cartoes_html[i]);
        //         }
        //         $('.request-card:nth-child(1)').removeClass('active');
        //         $('.request-card:nth-child(2)').addClass('active');
        //     } else {
        //         $('.request-card:nth-child(1)').addClass('active');
        //     }
        //
        //     $('#loading').fadeOut('slow');
        // } else {
        //     $('#loading').fadeOut('slow');
        // }
        // $(".remover-cartao").on('click', function (e) {
        //
        //     var el = $(this).next();
        //     var numero_cartao = $(this).data('numero');
        //     var bandeira = $(this).data('bandeira');
        //
        //     Swal.fire({
        //         title: 'Deseja excluir este cartão?',
        //         icon: 'question',
        //         html: '<Br><div class="jp-card-container mx-auto select-card">' + el.html() + '<br></div>',
        //         showCancelButton: true,
        //         confirmButtonColor: '#3085d6',
        //         cancelButtonColor: '#d33',
        //         confirmButtonText: 'Excluir',
        //         cancelButtonText: 'Cancelar',
        //         showClass: {
        //             popup: 'animated fadeInDown faster'
        //         },
        //         hideClass: {
        //             popup: 'animated fadeOutUp faster'
        //         },
        //     }).then((result) => {
        //         if (result.value) {
        //
        //             var tokenCartao = $(this).next('.select-card').data('token-card');
        //
        //             Swal.fire({
        //                 title: 'Processando',
        //                 allowEscapeKey: false,
        //                 showCancelButton: false,
        //                 showConfirmButton: false,
        //                 allowOutsideClick: false,
        //                 closeOnClickOutside: false,
        //                 onBeforeOpen: () => {
        //                     Swal.showLoading()
        //                 }
        //             });
        //             removerCartao(tokenCartao).then(data => {
        //
        //                 if (data.success === true) {
        //                     $(this).closest('.general-card').remove();
        //
        //                     swetalert('sucesso', data.message);
        //                 } else {
        //                     //erro
        //                     mensagem = data.message;
        //                     swetalert('erro', mensagem);
        //                 }
        //
        //             }, fail => {
        //                 swetalert('erro', fail);
        //             });
        //         }
        //     });
        // });
        // $(".select-card").on('click', function (e) {
        //     var el = $(this);
        //     $(this).find('.remover-cartao').hide();
        //     Swal.fire({
        //         title: 'Confirme a compra com o cartão?',
        //         text: "Ao confirmar a compra do(s) certificados será realizada!",
        //         icon: 'question',
        //         html: '<div class="jp-card-container mx-auto select-card">' + el.html() + '</div>',
        //         showCancelButton: true,
        //         confirmButtonColor: '#3085d6',
        //         cancelButtonColor: '#d33',
        //         confirmButtonText: 'Efetuar compra!',
        //         cancelButtonText: 'Cancelar',
        //         showClass: {
        //             popup: 'animated fadeInDown faster'
        //         },
        //         hideClass: {
        //             popup: 'animated fadeOutUp faster'
        //         },
        //     }).then((result) => {
        //         if (result.value) {
        //             var token_card = $(this).data('token-card');
        //             var cpfcnpj = $(this).data('document-card');
        //             var flag = $(this).data('card-flag');
        //             var transacao = localStorage.getItem('hash');
        //
        //             history.pushState(null, null, location.href);
        //             window.onpopstate = function () {
        //                 history.go(1);
        //             };
        //             Swal.fire({
        //                 title: 'Processando',
        //                 allowEscapeKey: false,
        //                 showCancelButton: false,
        //                 showConfirmButton: false,
        //                 allowOutsideClick: false,
        //                 closeOnClickOutside: false,
        //                 onBeforeOpen: () => {
        //                     Swal.showLoading()
        //                 }
        //             });
        //
        //             comprar_cartao_salvo(token_card, transacao, cpfcnpj, flag).then(data => {
        //
        //                 Swal.close({
        //                     showClass: {
        //                         popup: 'animated fadeInDown faster'
        //                     },
        //                     hideClass: {
        //                         popup: 'animated fadeOutUp faster'
        //                     }
        //                 });
        //
        //                 if (data.success === true) {
        //
        //                     // NOTIFICA O APP DA WEBVIEW
        //                     if (window.ReactNativeWebView !== undefined && window.ReactNativeWebView.postMessage !== undefined) {
        //                         window.ReactNativeWebView.postMessage(1);
        //                     }
        //
        //                     $('.contentAll').remove();
        //                     $('body').addClass('d-flex justify-content-center align-items-center');
        //                     $('.info-concluido').removeClass('d-none');
        //                     $('.info-concluido').addClass('d-flex');
        //                 } else {
        //                     mensagem = data.message;
        //                     //erro
        //                     $('.contentAll').remove();
        //                     $('body').addClass('d-flex justify-content-center align-items-center');
        //                     $('.info-rejeitado').removeClass('d-none');
        //                     $('.info-rejeitado h2 b').text(mensagem);
        //                     $('.info-rejeitado').addClass('d-flex');
        //                 }
        //
        //             }, fail => {
        //                 mensagem = data.message;
        //                 //erro
        //                 $('.contentAll').remove();
        //                 $('body').addClass('d-flex justify-content-center align-items-center');
        //                 $('.info-rejeitado').removeClass('d-none');
        //                 $('.info-rejeitado h2 b').text(mensagem);
        //                 $('.info-rejeitado').addClass('d-flex');
        //                 swetalert('erro', fail);
        //             });
        //
        //         } else if (result.dismiss === Swal.DismissReason.cancel) {
        //             $(this).find('.remover-cartao').show();
        //         }
        //     });
        // });

        // hover botao excluir
        // $(document).on('mouseover','.col-card', function (event) {
        //     var element = $(this);
        //     element.find('.remover-cartao').fadeIn();
        // }).on('mouseleave','.col-card',  function(){
        //     var element = $(this).parent();
        //     element.find('.remover-cartao').fadeOut();
        // });

    });
    // } else {
    //     swetalert('erro', response);
    // }
}

function closeInfo() {

    $('#loading').fadeIn('slow');
    $('#infoPagSorte').hide();
    buscaDadosHash();
}

function getTokenAixmobil() {

    return new Promise((resolve, reject) => {
        aix.setPublicKey(varTokenPublico);
        aix.setHostName(varHostname);

        aix.getTransactionToken().then(function (Trasactiontoken) {
            if (Trasactiontoken) {
                tokenAixmobil = Trasactiontoken;
                resolve(true);
            }
        }, function () {
            // console.log(error);
            const msg = "<h1 style='text-align: center'>Atenção! Por questões de segurança não foi possível prosseguir com a compra. Por favor tente novamente!</h1>";
            $('body').html(msg);
            reject(false);
        });
    });
}

$(window).on('load', function () {
    // token transacao AIX


});

function preencheDadosCliente(resp) {
    return new Promise((resolve, reject) => {
        var dados = JSON.parse(resp);

        var nomeComposto = dados.nome.split(' ');

        if (nomeComposto[0] && nomeComposto[0] !== '') {
            $("#nome").val(nomeComposto[0]);
            $("#juno-nome").val(nomeComposto[0]);
            $("#assinar").attr('nome', nomeComposto[0]);
        }
        if (nomeComposto[1] && nomeComposto[1] !== '') {
            $("#sobrenome").val(nomeComposto[1]);
            $("#juno-sobrenome").val(nomeComposto[1]);
            $("#assinar").attr('sobrenome', nomeComposto[1]);

        }
        if (dados.email && dados.email !== '') {
            $("#email").val(dados.email);
            $("#juno-email").val(dados.email);
            $("#assinar").attr('email', dados.email);
        }
        if (dados.cpf && dados.cpf !== '') {
            $("#cpf").val(dados.cpf);
            $("#juno-cpf").val(dados.cpf);
            $("#assinar").attr('cpf', dados.cpf);
        }
        if (dados.logradouro && dados.logradouro !== '') {
            $("#endereco").val(dados.logradouro);
            $("#juno-endereco").val(dados.logradouro);
            $("#assinar").attr('logradouro', dados.logradouro);
        }
        if (dados.numero && dados.numero !== '') {
            $("#numero").val(dados.numero);
            $("#juno-numero").val(dados.numero);
            $("#assinar").attr('numero', dados.numero);
        }
        if (dados.complemento && dados.complemento !== '') {
            $("#complemento").val(dados.complemento);
            $("#juno-complemento").val(dados.complemento);
            $("#assinar").attr('complemento', dados.complemento);
        }
        if (dados.bairro && dados.bairro !== '') {
            $("#bairro").val(dados.bairro);
            $("#juno-bairro").val(dados.bairro);
            $("#assinar").attr('bairro', dados.bairro);
        }
        if (dados.cidade && dados.cidade !== '') {
            $("#cidade").val(dados.cidade);
            $("#juno-cidade").val(dados.cidade);
            $("#assinar").attr('cidade', dados.cidade);
        }
        if (dados.cep && dados.cep !== '') {
            $("#cep").val(dados.cep);
            $("#juno-cep").val(dados.cep);
            $("#cep").blur();
            $("#assinar").attr('cep', dados.cep);
        }
        var data_nascimento = dados.nascimento.split(' ');
        if (dados.nascimento && dados.nascimento !== '') {
            $("#data_nascimento").val(data_nascimento[0]);
            $("#juno-data_nascimento").val(data_nascimento[0]);
            $("#assinar").attr('cep', data_nascimento[0]);
        }
        if (dados.telefone && dados.telefone !== '') {
            $("#telefone").val(dados.telefone);
            $("#juno-telefone").val(dados.telefone);
        }
        resolve(true);
    });
}

// ação do click do botão de assinatura
$("#assinar").on("click", function (e) {
    e.preventDefault();
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
    const inputMesValidade = $("#mes-validade").val();
    const inputAnoValidade = $("#ano-validade").val();

    if (inputMesValidade === '') {
        swetalert('informacao', 'Selecione o mês de validade.')
        return;
    }
    if (inputAnoValidade === '') {
        swetalert('informacao', 'Selecione o ano de validade.')
        return;
    }
    var campos;
    Swal.fire({
        title: 'Processando',
        allowEscapeKey: false,
        button: false,
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        onBeforeOpen: () => {
            Swal.showLoading()
        }
    });
    var elmForm = $("#form-pagamento");
    if (elmForm) {
        elmForm.validator('validate');
        var elmErr = elmForm.find('.has-error');
        campos = validaCamposVazios(elmErr);
        if (elmErr && elmErr.length > 0 && campos.length > 1) {
            campos = validaCamposVazios(elmErr);
            var total = campos.length
            var mensagem = '';
            switch (true) {
                case (total === 1):
                    mensagem = 'O campo ' + campos[0] + ' é obrigatório.';
                    break;
                case (total > 1):
                    mensagem = 'Os campos ' + campos.join(', ') + ' são obrigatórios';
                    break;
            }
            swetalert('erro', mensagem);

            return false;
        } else {
            valida_campos();
            const concluido = $('.info-concluido');
            const rejeitado = $('.info-rejeitado');
            checkout().then(data => {
                Swal.close({
                    showClass: {
                        popup: 'animated fadeInDown faster'
                    },
                    hideClass: {
                        popup: 'animated fadeOutUp faster'
                    }
                });

                if (data.success === true && data.data !== undefined && data.data.urlautenticacao !== undefined) {
                    location = data.data.urlautenticacao;
                } else {
                    if (data.success === true) {

                        // NOTIFICA O APP DA WEBVIEW
                        if (window.ReactNativeWebView !== undefined && window.ReactNativeWebView.postMessage !== undefined) {
                            window.ReactNativeWebView.postMessage(1);
                        }

                        $('.contentAll').remove();
                        $('body').addClass('d-flex justify-content-center align-items-center');
                        concluido.removeClass('d-none');
                        concluido.addClass('d-flex');
                    } else {
                        mensagem = data.message;
                        //erro
                        $('.contentAll').remove();
                        $('body').addClass('d-flex justify-content-center align-items-center');
                        rejeitado.removeClass('d-none');
                        $('.info-rejeitado h2 b').text(mensagem);
                        rejeitado.addClass('d-flex');
                    }
                }
            }, fail => {
                mensagem = data.message;
                //erro
                $('.contentAll').remove();
                $('body').addClass('d-flex justify-content-center align-items-center');
                rejeitado.removeClass('d-none');
                $('.info-rejeitado h2 b').text(mensagem);
                rejeitado.addClass('d-flex');
                swetalert('erro', fail);
            });
            return false;
        }
    }
});

// $('#juno-nome').val('João Gonçalves');
// $('#juno-sobrenome').val('Bila');
// $('#juno-email').val('joaogsa@outlook.com');
// $('#juno-telefone').val('(81)999827795');
// $('#juno-cpf').val('08355814460');
// $('#juno-endereco').val('av. Rodolfo Aureliano');
// $('#juno-numero').val('123');
// $('#juno-bairro').val('Vila Torres Galvao');
// $('#juno-cidade').val('Paulista');
// $('#juno-estado').val('PE');
// $('#juno-pais').val('BRA');
// $('#juno-cep').val('53403740');

// Dados do Cartao
// $('#juno-nome_titular').val($('#juno-nome').val());
// $('#juno-numero_cartao').val('5544 0463 9414 4542');
// $('#juno-mes-validade').val('03');
// $('#juno-ano-validade').val('2022');
// $('#juno-codigo').val('362');
// $('#juno-data_nascimento').val('10/04/1989');

$("#comprar-juno").on("click", function (e) {


    e.preventDefault();
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
    const inputMesValidade = $("#juno-mes-validade").val();
    const inputAnoValidade = $("#juno-ano-validade").val();

    if (inputMesValidade === '') {
        swetalert('informacao', 'Selecione o mês de validade.')
        return;
    }
    if (inputAnoValidade === '') {
        swetalert('informacao', 'Selecione o ano de validade.')
        return;
    }

    mostrarDivCarregando();

    const config = {};
    $("#form-cadastro-juno").submit();
    let isValid = validatorJuno.valid();
    if (isValid) {

        $("#form-cadastro-juno").serializeArray().map(function (item) {
            if (config[item.name]) {
                if (typeof (config[item.name]) === "string") {
                    config[item.name] = [config[item.name]];
                }
                config[item.name].push(item.value);
            } else {
                config[item.name] = item.value;
            }
        });

        const concluido = $('.info-concluido');
        const rejeitado = $('.info-rejeitado');
        nova_compra_juno().then(dataRequest => {

            enviaCheckoutJuno(dataRequest);

        }, fail => {
            console.log(fail, 'falha juno');
            esconderDivCarregando();
            $('.contentAll').remove();
            $('body').addClass('d-flex justify-content-center align-items-center');
            rejeitado.removeClass('d-none');
            $('.info-rejeitado h2 b').text('Não foi possível concluir sua compra.');
            rejeitado.addClass('d-flex');
            // swetalert('erro', fail);
        });
    } else {

        Swal.fire(
            'Atencão!',
            'Preencha todos os campos obrigatórios e tente novamente!',
            'error'
        );
        esconderDivCarregando();
    }
});

function enviaCheckoutJuno(dataRequest) {

    const concluido = $('.info-concluido');
    const rejeitado = $('.info-rejeitado');

    $.ajaxSetup({
        headers: {
            'AccessToken': localStorage.getItem('dt')
        }
    });
    $.ajax({
        'type': 'POST',
        'url': postCheckoutJuno,
        'contentType': 'application/json',
        'data': JSON.stringify(dataRequest),
        'dataType': 'json',
    }).done(function (response) {
        if(response.success === true) {
            $('.contentAll').remove();
            $('body').addClass('d-flex justify-content-center align-items-center');
            concluido.removeClass('d-none');
            concluido.addClass('d-flex');
            setCsrfStorage(response);
        } else {
            $('.contentAll').remove();
            $('body').addClass('d-flex justify-content-center align-items-center');
            rejeitado.removeClass('d-none');
            $('.info-rejeitado h2 b').text('Não foi possível concluir a compra.');
            rejeitado.addClass('d-flex');
        }

        esconderDivCarregando();
    }).fail(function () {
        $('.contentAll').remove();
        $('body').addClass('d-flex justify-content-center align-items-center');
        rejeitado.removeClass('d-none');
        $('.info-rejeitado h2 b').text('Não foi possível concluir a compra.');
        rejeitado.addClass('d-flex');
        esconderDivCarregando();
    });


    // Swal.close({
    //     showClass: {
    //         popup: 'animated fadeInDown faster'
    //     },
    //     hideClass: {
    //         popup: 'animated fadeOutUp faster'
    //     }
    // });
    // console.log(data, 'sucesso!');
    //
    // if (data.success === true && data.data !== undefined && data.data.urlautenticacao !== undefined) {
    //     location = data.data.urlautenticacao;
    // } else {
    //     if (data.success === true) {
    //
    //         // NOTIFICA O APP DA WEBVIEW
    //         if (window.ReactNativeWebView !== undefined && window.ReactNativeWebView.postMessage !== undefined) {
    //             window.ReactNativeWebView.postMessage(1);
    //         }
    //
    //         $('.contentAll').remove();
    //         $('body').addClass('d-flex justify-content-center align-items-center');
    //         concluido.removeClass('d-none');
    //         concluido.addClass('d-flex');
    //     } else {
    //         mensagem = data.message;
    //         //erro
    //         $('.contentAll').remove();
    //         $('body').addClass('d-flex justify-content-center align-items-center');
    //         rejeitado.removeClass('d-none');
    //         $('.info-rejeitado h2 b').text(mensagem);
    //         rejeitado.addClass('d-flex');
    //     }
    // }
}

function checkout() {


    // dados do cartão //
    var nomeTitular = $('#nome_titular').val();
    var cartao = $("#numero_cartao").val().split(' ').join("");
    var cvv = $("#codigo").val();
    var bandeira = $("#bandeira").val();
    var validadeAno = $("#ano-validade").val();
    var validadeMes = $("#mes-validade").val();
    var salvar_card = $('#save-card').is(':checked');
    var tipo_cartao = $('input[name="forma_pagamento"]:checked').val();
    const validade = validadeMes + '/' + validadeAno;

    if (tipo_cartao === '1') {
        tipo_cartao = 'c'
    } else {
        tipo_cartao = 'd'
    }
    if (salvar_card) {
        salvar_card = 's'
    } else {
        salvar_card = 'n'
    }

    // dados pessoais
    var email = $("#email").val();
    var telefone = $("#telefone").val().replace(/[^0-9\s]+/g, "").split(" ").join("");
    var cpf = $("#cpf").val().replace(/[^0-9\s]+/g, "");
    var hastag = $(".hastag-da-sorte-newcard").val();

    let checkbox = $("#check-doacao");
    let isChecked = checkbox.is(":checked");

    const config = {};

    if (!isChecked) {

        let validator = validarFormulario();
        let isValid = validator.valid();
        if (isValid) {
            // $('#form-cadastro')[0]

            $("#form-cadastro").serializeArray().map(function (item) {
                if (config[item.name]) {
                    if (typeof (config[item.name]) === "string") {
                        config[item.name] = [config[item.name]];
                    }
                    config[item.name].push(item.value);
                } else {
                    config[item.name] = item.value;
                }
            });
        }
    }

    return new Promise((resolve, reject) => {
        $.ajaxSetup({
            headers: {
                'AccessToken': localStorage.getItem('dt'),
                'CsrfToken': localStorage.getItem('dtcsrf')
            }
        });

        getTokenAixmobil().then(() => {
            var data = {
                email: email,
                cpfCnpjPortador: cpf,
                telefone: telefone,
                cartao: {
                    cadastrar_cartao: salvar_card,
                    tipo_cartao: tipo_cartao,
                    cartao: aix.encryptValue(cartao),
                    bandeira: bandeira,
                    portador: nomeTitular,
                    cvv: aix.encryptValue(cvv),
                    validade: aix.encryptValue(validade),
                },
                transacao: localStorage.getItem('hash'), // hash do bilhete
                codigoPromocional: hastag,
                checkoutSessionToken: tokenAixmobil,
                inputs: config
            };

            $.ajax({
                'type': 'POST',
                'url': postCheckout,
                'contentType': 'application/json',
                'data': JSON.stringify(data),
                'dataType': 'json',
            }).done(function (response) {

                setCsrfStorage(response);
                resolve(response);
            }).fail(function () {
                reject({success: false, message: 'Não foi possível conectar ao servidor!'})
            });


        }, () => {
            const msg = "<h1 style='text-align: center'>Atenção! Por questões de segurança não foi possível prosseguir com a compra. Por favor tente novamente!</h1>";
            $('body').html(msg);
            reject({success: false, message: 'Não foi possível conectar ao servidor!!'})
        });
    });


}

// function comprar_cartao_salvo(token_card, transacao, cpfcnpj, cartao) {
//
//     var hastag = $(".hastag-da-sorte-cardsaved").val();
//     var data = {
//         token_card: token_card,
//         transacao: transacao,
//         cpfCnpjPortador: cpfcnpj,
//         codigoPromocional: hastag,
//         flag: cartao
//     };
//
//     return new Promise((resolve, reject) => {
//         $.ajaxSetup({
//             headers:{
//                 'AccessToken': localStorage.getItem('dt'),
//                 'CsrfToken': localStorage.getItem('dtcsrf')
//             }
//         });
//         $.ajax({
//             'type': 'POST',
//             'url': postCheckoutToken,
//             'contentType': 'application/json',
//             'data': JSON.stringify(data),
//             'dataType': 'json',
//         }).done(function (response) {
//
//             setCsrfStorage(response);
//             resolve(response);
//         }).fail(function () {
//             reject({success: false, message: 'Não foi possível conectar ao servidor!'})
//         });
//     });
// }

function removerCartao(tokenCartao) {
    var data = {
        tokenCartao: tokenCartao
    };
    return new Promise((resolve, reject) => {
        $.ajaxSetup({
            headers: {
                'AccessToken': localStorage.getItem('dt'),
                'CsrfToken': localStorage.getItem('dtcsrf')
            }
        });
        $.ajax({
            'type': 'POST',
            'url': postremoverCartao,
            'contentType': 'application/json',
            'data': JSON.stringify(data),
            'dataType': 'json',
        }).done(function (response) {

            setCsrfStorage(response);
            resolve(response);
        }).fail(function () {
            reject({success: false, message: 'Não foi possível conectar ao servidor!'})
        });
    });
}

var card = new Card({
    form: '#form-pagamento',
    container: '.card-wrapper',
    formSelectors: {
        numberInput: '#numero_cartao', // optional — default input[name="number"]
        expiryInput: '#validade', // optional — default input[name="expiry"]
        cvcInput: '#codigo', // optional — default input[name="cvc"]
        nameInput: '#nome_titular' // optional - defaults input[name="name"]
    },
    width:
        0,
    debug: true
});

var cardJuno = new Card({
    form: '#form-cadastro-juno',
    container: '.card-wrapper-juno',
    formSelectors: {
        numberInput: '#juno-numero_cartao', // optional — default input[name="number"]
        expiryInput: '#validade', // optional — default input[name="expiry"]
        cvcInput: '#juno-codigo', // optional — default input[name="cvc"]
        nameInput: '#juno-nome_titular' // optional - defaults input[name="name"]
    },
    width:
        0,
    debug: true
});

function swetalert(tipo, mensagem = null, titulo = null) {
    switch (tipo) {
        case 'erro':

            return Swal.fire({
                icon: 'error',
                title: 'Erro',
                text: mensagem,
                showClass: {
                    popup: 'animated fadeInDown faster'
                },
                hideClass: {
                    popup: 'animated fadeOutUp faster'
                },
                footer: '',
            });
            break
        case 'sucesso':
            return Swal.fire({
                icon: 'success',
                showCloseButton: true,
                title: 'Sucesso!',
                text: mensagem,
                showClass: {
                    popup: 'animated fadeInDown faster'
                },
                hideClass: {
                    popup: 'animated fadeOutUp faster'
                }
            });
            break
        case 'informacao':
            return Swal.fire({
                icon: 'info',
                showCloseButton: true,
                title: 'Atenção',
                text: mensagem,
                showClass: {
                    popup: 'animated fadeInDown faster'
                },
                hideClass: {
                    popup: 'animated fadeOutUp faster'
                },
                footer: 'Verifique os dados e efetue a compra novamente.'
            })
            break
        case 'tempo':
            return Swal.fire({
                title: 'Processando',
                allowEscapeKey: false,
                showCancelButton: false,
                showConfirmButton: false,
                allowOutsideClick: false,
                closeOnClickOutside: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            });
            break;
        case 'info':
            return Swal.fire({
                icon: 'info',
                title: 'Atenção!',
                html: mensagem,
                showClass: {
                    popup: 'animated fadeInDown faster'
                },
                hideClass: {
                    popup: 'animated fadeOutUp faster'
                },

            });
            break
    }

}
