$(window).bind('scroll', function () {
    if ($(window).scrollTop() > 1) {
        $('#topo-mobile').addClass('fixed-header');
        $('#topo-site').addClass('fixed-header');
    } else {
        $('#topo-mobile').removeClass('fixed-header');
        $('#topo-site').removeClass('fixed-header');
    }
});


$("#expandir-video").on("click", function(){
	$("#blur").addClass('blurAtivo');
	$("#playerAovivo").show();
    $("#targetAovivo").hide();
});

$("#fechar-video").on("click", function(){
    var showAovivo = $("#show-aovivo");
    var targetAovivo = $("#targetAovivo");
    var status = $("#show-aovivo .status");

    targetAovivo.css('height','34px');
    showAovivo.css('cursor', 'pointer');
    status.addClass('fechado');
});

$("#show-aovivo .status").on("click", function(){
    if( $(this).hasClass("fechado") ){
        var showAovivo = $("#show-aovivo");
        var targetAovivo = $("#targetAovivo");
        var status = $("#show-aovivo .status");

        targetAovivo.css('height','330px');
        showAovivo.css('cursor', 'default');
        status.removeClass('fechado');
    }
});

$("#fecharAovivo").on("click", function(){
	$("#blur").removeClass('blurAtivo');
	$("#playerAovivo").hide();
    $("#targetAovivo").show();
})

$("#fechar-target-aovivo").on("click", function(){
    var target = $("#targetAovivo");
    var abrirTarget = $("#abrirTargetAovivo");

    target.addClass('slideOutDown');
    setTimeout(function(){
        target.hide();

        abrirTarget.show();
        if( abrirTarget.hasClass('slideOutDown') ){
            abrirTarget.removeClass('slideOutDown');
        }
        abrirTarget.addClass('slideInUp');
    },1000);
});

$("#abrirTargetAovivo").on("click", function(){
    var target = $("#targetAovivo");
    var abrirTarget = $("#abrirTargetAovivo");

    abrirTarget.addClass('slideOutDown');

    setTimeout(function(){
        abrirTarget.hide();
        target.show();
        target.removeClass('slideOutDown');
        target.addClass('slideInUp');
    },1000);
});

function mostrarContador(dia_sorteio, hora_inicio_contador, hora_inicio_sorteio, hora_final_sorteio){

    console.log('contador');

    dia_sorteio = parseInt(dia_sorteio);

    hora_inicio_contador = hora_inicio_contador.split(':');
    hora_inicio_contador = parseInt(hora_inicio_contador[0]+hora_inicio_contador[1]);

    hora_inicio_sorteio = hora_inicio_sorteio.split(':');
    hora_inicio_sorteio = parseInt(hora_inicio_sorteio[0]+hora_inicio_sorteio[1]);

    hora_final_sorteio = hora_final_sorteio.split(':');
    hora_final_sorteio = parseInt(hora_final_sorteio[0]+hora_final_sorteio[1]);

    var agora = new Date();

    var diaAtual  = agora.getDay();
    var horaAtual = ('0'+agora.getHours()).slice(-2);
    var minAtual  = ('0'+agora.getMinutes()).slice(-2);

    horaAtual = parseInt(horaAtual+minAtual);

    if(diaAtual == dia_sorteio && horaAtual >= hora_inicio_contador && horaAtual <= hora_final_sorteio){
        $("#targetAovivo").show();

        hora_inicio_sorteio = hora_inicio_sorteio.toString();

        if(hora_inicio_sorteio.length == 3){
            var hic = parseInt( hora_inicio_sorteio.substr(0,1) );
            var mic = parseInt( hora_inicio_sorteio.substr(1) );
        }else{
            var hic = parseInt( hora_inicio_sorteio.substr(0,2) );
            var mic = parseInt( hora_inicio_sorteio.substr(2) );
        }

        atualizaContador(agora.getFullYear(), agora.getMonth()+1, agora.getDate(), hic, mic, '00');
    }
}

function atualizaContador(YY, MM, DD, HH, MI, SS) {

    var hoje = new Date();
    var futuro = new Date(YY,MM-1,DD,HH,MI,SS);
    var ss = parseInt((futuro - hoje) / 1000);
    var mm = parseInt(ss / 60);
    var hh = parseInt(mm / 60);
    var dd = parseInt(hh / 24);
    ss = ss - (mm * 60);
    mm = mm - (hh * 60);
    hh = hh - (dd * 24);
    var faltam = '';
    faltam += (dd && dd > 1) ? dd+' dias, ' : (dd==1 ? '1 dia, ' : '');
    faltam += (toString(hh).length) ? hh+'h ' : '';
    faltam += (toString(mm).length) ? mm+'m e ' : '';
    faltam += ss+'s';

    var timerContainer = $(".countTimer");
    var targetAovivo   = $("#targetAovivo");
    var contador = $("#contador");
    var abrirAovivo = $("#abrirTargetAovivo");
    var showAovivo = $("#show-aovivo");

    if (dd+hh+mm+ss > 0) {
       timerContainer.empty();
       timerContainer.append(faltam);
       setTimeout(function(){ atualizaContador(YY, MM, DD, HH, MI, SS); },1000);
    }
    else{
        if( targetAovivo.hasClass('slideOutDown') ){
            targetAovivo.show();
            targetAovivo.removeClass('slideOutDown');
            targetAovivo.addClass('slideInUp');
        }

        targetAovivo.addClass('aovivo');
        showAovivo.addClass('animated slideInUp');

        contador.hide();
        abrirAovivo.hide();

        timerContainer.empty();
    }
}
