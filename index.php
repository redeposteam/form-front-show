<?php

header('Content-type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Pagamento</title>

  <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/fontawesome/css/all.min.css">
  <link rel="stylesheet" href="js/loading/dist/jquery.loading.min.css">
  <link rel="stylesheet" href="js/sweetalert2/sweetalert2.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous">
  <link rel="stylesheet" href="css/aos.css"/>
  <!-- animatecss -->
  <link rel="stylesheet" href="css/animate.css">
  <!-- Card Style -->
  <link rel="stylesheet" href="card/card.css">
  <style>

    .dadosUsuario {
      border-radius: 4px;
      border: 1px solid #9e9e9e;
      padding-top: 7px;
      padding-bottom: 7px;
    }

    .contentCertificado {
      border: 1px solid #b6ad0f;
      border-radius: 5px;
      padding: 12px;
      background: #f3f2b2;
    }


    /* The switch - the box around the slider */
    .switch {
      position: relative;
      display: inline-block;
      /*width: 60px;*/
      /*height: 34px;*/
      width: 50px;
      height: 25px;
    }

    /* Hide default HTML checkbox */
    .switch input {
      opacity: 0;
      width: 0;
      height: 0;
    }

    /* The slider */
    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }

    .slider:before {
      position: absolute;
      content: "";
      height: 17px;
      width: 17px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }

    input:checked + .slider {
      background-color: #1db92a;
    }

    input:focus + .slider {
      box-shadow: 0 0 1px #1db92a;
    }

    input:checked + .slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
      border-radius: 34px;
    }

    .slider.round:before {
      border-radius: 50%;
    }


    .form-group {
      margin-bottom: 0 !important;
    }

    .card {
      margin-top: 10px;
      margin-bottom: 10px;
    }

    .card img {
      max-width: 100%;
      max-height: 50px;
    }

    .cursor-pointer {
      cursor: pointer;
    }

    .border-juno {
      border-color: #252AFF !important;
    }

  </style>
</head>

<body>

<!-- Conteudo principal -->
<div id="conteudo" class="container py-5" data-aos="zoom-in" style="display: none">
  <form id="form-cadastro" method="post">
    <div class="row">
      <div class="col"></div>
      <div class="col-md-3 text-center">
        <label class="switch">
          <input id="check-doacao" CHECKED type="checkbox">
          <span class="slider round"></span>
        </label>
        <label for="check-doacao">
          ACEITO DOAR</label>
      </div>
      <div class="col"></div>
    </div>
    <div id="info-content" style="display: none;">
      <h4>Identificação</h4>
      <hr>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Nome Completo</label>
            <input type="text" name="nomeCompleto" class="form-control" required>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Gênero</label>
            <div class="row">
              <div class="col">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="genero" id="generoM" value="M"
                         checked required>
                  <label class="form-check-label" for="generoM">Masculino</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="genero" id="generoF" value="F"
                         required>
                  <label class="form-check-label" for="generoF">Feminino</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Naturalidade</label>
            <input type="text" class="form-control" name="naturalidade" required>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Data de Nascimento</label>
            <input type="text" name="dataNascimento" class="form-control" id="dataNascimento" required>
            <span class="danger"></span>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>CPF</label>
            <input type="text" name="cpf" class="form-control" required>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>Identidade</label>
            <input type="text" name="rg" id="identidade" class="form-control" required>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Órgão Emissor</label>
            <input type="text" name="orgao" class="form-control" required>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Estado</label>
            <select name="estado" class="form-control" required>

              <option value="">Selecione</option>
              <option value="AC">Acre (AC)</option>
              <option value="AL">Alagoas (AL)</option>
              <option value="AP">Amapá (AP)</option>
              <option value="AM">Amazonas (AM)</option>
              <option value="BA">Bahia (BA)</option>
              <option value="CE">Ceará (CE)</option>
              <option value="DF">Distrito Federal (DF)</option>
              <option value="ES">Espírito Santo (ES)</option>
              <option value="GO">Goiás (GO)</option>
              <option value="MA">Maranhão (MA)</option>
              <option value="MT">Mato Grosso (MT)</option>
              <option value="MS">Mato Grosso do Sul (MS)</option>
              <option value="MG">Minas Gerais (MG)</option>
              <option value="PA">Pará (PA)</option>
              <option value="PB">Paraíba (PB)</option>
              <option value="PR">Paraná (PR)</option>
              <option value="PE">Pernambuco (PE)</option>
              <option value="PI">Piauí (PI)</option>
              <option value="RJ">Rio de Janeiro (RJ)</option>
              <option value="RN">Rio Grande do Norte (RN)</option>
              <option value="RS">Rio Grande do Sul (RS)</option>
              <option value="RO">Rondônia (RO)</option>
              <option value="RR">Roraima (RR)</option>
              <option value="SC">Santa Catarina (SC)</option>
              <option value="SP">São Paulo (SP)</option>
              <option value="SE">Sergipe (SE)</option>
              <option value="TO">Tocantins (TO)</option>

            </select>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Data da Emissão</label>
            <input type="text" name="dataEmissao" id="dataEmissao" class="form-control" required>
          </div>
        </div>

      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>Nome da Mãe</label>
            <input type="text" name="nomeMae" class="form-control" required>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Nome do Pai</label>
            <input type="text" name="nomePai" class="form-control" required>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Estado Civil</label>
            <select name="estadoCivil" class="form-control" required>

              <option value="">Selecione</option>
              <option value="C">Casado</option>
              <option value="S">Solteiro</option>

            </select>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Profissão</label>
            <input type="text" name="profissao" class="form-control" required>
          </div>
        </div>
      </div>

      <h4>Endereço</h4>
      <hr>

      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>CEP</label>
            <input type="text" name="cep" id="cep" class="form-control" required>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Logradouro</label>
            <input type="text" name="logradouro" class="form-control" required>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Número</label>
            <input type="text" name="numero" id="numeroResidencia" class="form-control" required>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Complemento</label>
            <input type="text" name="complemento" class="form-control">
          </div>
        </div>
        <div class="col">
          <div class="form-group">
            <label>Bairro</label>
            <input type="text" name="bairro" class="form-control" required>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>Cidade</label>
            <input type="text" name="cidade" class="form-control" required>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>UF</label>
            <select name="uf" class="form-control" required>
              <option value="">Selecione</option>
              <option value="AC">Acre (AC)</option>
              <option value="AL">Alagoas (AL)</option>
              <option value="AP">Amapá (AP)</option>
              <option value="AM">Amazonas (AM)</option>
              <option value="BA">Bahia (BA)</option>
              <option value="CE">Ceará (CE)</option>
              <option value="DF">Distrito Federal (DF)</option>
              <option value="ES">Espírito Santo (ES)</option>
              <option value="GO">Goiás (GO)</option>
              <option value="MA">Maranhão (MA)</option>
              <option value="MT">Mato Grosso (MT)</option>
              <option value="MS">Mato Grosso do Sul (MS)</option>
              <option value="MG">Minas Gerais (MG)</option>
              <option value="PA">Pará (PA)</option>
              <option value="PB">Paraíba (PB)</option>
              <option value="PR">Paraná (PR)</option>
              <option value="PE">Pernambuco (PE)</option>
              <option value="PI">Piauí (PI)</option>
              <option value="RJ">Rio de Janeiro (RJ)</option>
              <option value="RN">Rio Grande do Norte (RN)</option>
              <option value="RS">Rio Grande do Sul (RS)</option>
              <option value="RO">Rondônia (RO)</option>
              <option value="RR">Roraima (RR)</option>
              <option value="SC">Santa Catarina (SC)</option>
              <option value="SP">São Paulo (SP)</option>
              <option value="SE">Sergipe (SE)</option>
              <option value="TO">Tocantins (TO)</option>
            </select>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Telefone</label>
            <input type="text" name="telefone" id="telefoneCadastro" class="form-control" required>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Telefone Recado</label>
            <input type="text" name="telefoneRecado" id="telefoneRecado" class="form-control">
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" required>
          </div>
        </div>
      </div>

      <h4>Banco</h4>
      <hr>

      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>Nome do Titular</label>
            <input type="text" name="nomeTitularBanco" class="form-control" required>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Banco</label>
            <select name="banco" class="form-control" required>
              <option value="">Selecione</option>
              <option value="332">332 Acesso Soluções de Pagamento S.A.</option>
              <option value="117">117 ADVANCED CORRETORA DE CÂMBIO LTDA</option>
              <option value="272">272 AGK CORRETORA DE CAMBIO S.A.</option>
              <option value="349">349 AMAGGI S.A. - CRÉDITO, FINANCIAMENTO E INVESTIMENTO</option>
              <option value="188">188 ATIVA INVESTIMENTOS S.A. CORRETORA DE TÍTULOS, CÂMBIO E VALORES
              </option>
              <option value="280">280 Avista S.A. Crédito, Financiamento e Investimento</option>
              <option value="80">80 B&T CORRETORA DE CAMBIO LTDA.</option>
              <option value="654">654 Banco A.J.Renner S.A.</option>
              <option value="246">246 Banco ABC Brasil S.A.</option>
              <option value="75">75 Banco ABN AMRO S.A.</option>
              <option value="121">121 Banco Agibank S.A.</option>
              <option value="25">25 Banco Alfa S.A.</option>
              <option value="65">65 Banco Andbank (Brasil) S.A.</option>
              <option value="213">213 Banco Arbi S.A.</option>
              <option value="96">96 Banco B3 S.A.</option>
              <option value="24">24 Banco BANDEPE S.A.</option>
              <option value="330">330 Banco Bari de Investimentos e Financiamentos S/A</option>
              <option value="318">318 Banco BMG S.A.</option>
              <option value="752">752 Banco BNP Paribas Brasil S.A.</option>
              <option value="107">107 Banco BOCOM BBM S.A.</option>
              <option value="63">63 Banco Bradescard S.A.</option>
              <option value="36">36 Banco Bradesco BBI S.A.</option>
              <option value="122">122 Banco Bradesco BERJ S.A.</option>
              <option value="394">394 Banco Bradesco Financiamentos S.A.</option>
              <option value="237">237 Banco Bradesco S.A.</option>
              <option value="218">218 Banco BS2 S.A.</option>
              <option value="208">208 Banco BTG Pactual S.A.</option>
              <option value="336">336 Banco C6 S.A.</option>
              <option value="473">473 Banco Caixa Geral - Brasil S.A.</option>
              <option value="412">412 Banco Capital S.A.</option>
              <option value="40">40 Banco Cargill S.A.</option>
              <option value="739">739 Banco Cetelem S.A.</option>
              <option value="233">233 Banco Cifra S.A.</option>
              <option value="745">745 Banco Citibank S.A.</option>
              <option value="241">241 Banco Clássico S.A.</option>
              <option value="756">756 Banco Cooperativo do Brasil S.A. - BANCOOB</option>
              <option value="748">748 Banco Cooperativo Sicredi S.A.</option>
              <option value="222">222 Banco Credit Agricole Brasil S.A.</option>
              <option value="505">505 Banco Credit Suisse (Brasil) S.A.</option>
              <option value="69">69 Banco Crefisa S.A.</option>
              <option value="266">266 Banco Cédula S.A.</option>
              <option value="3">3 Banco da Amazônia S.A.</option>
              <option value="83">83 Banco da China Brasil S.A.</option>
              <option value="707">707 Banco Daycoval S.A.</option>
              <option value="300">300 Banco de La Nacion Argentina</option>
              <option value="495">495 Banco de La Provincia de Buenos Aires</option>
              <option value="335">335 Banco Digio S.A.</option>
              <option value="1">1 Banco do Brasil S.A.</option>
              <option value="47">47 Banco do Estado de Sergipe S.A.</option>
              <option value="37">37 Banco do Estado do Pará S.A.</option>
              <option value="41">41 Banco do Estado do Rio Grande do Sul S.A.</option>
              <option value="4">4 Banco do Nordeste do Brasil S.A.</option>
              <option value="265">265 Banco Fator S.A.</option>
              <option value="224">224 Banco Fibra S.A.</option>
              <option value="626">626 Banco Ficsa S.A.</option>
              <option value="94">94 Banco Finaxis S.A.</option>
              <option value="612">612 Banco Guanabara S.A.</option>
              <option value="12">12 Banco Inbursa S.A.</option>
              <option value="604">604 Banco Industrial do Brasil S.A.</option>
              <option value="653">653 Banco Indusval S.A.</option>
              <option value="77">77 Banco Inter S.A.</option>
              <option value="249">249 Banco Investcred Unibanco S.A.</option>
              <option value="479">479 Banco ItauBank S.A</option>
              <option value="184">184 Banco Itaú BBA S.A.</option>
              <option value="29">29 Banco Itaú Consignado S.A.</option>
              <option value="376">376 Banco J. P. Morgan S.A.</option>
              <option value="74">74 Banco J. Safra S.A.</option>
              <option value="217">217 Banco John Deere S.A.</option>
              <option value="76">76 Banco KDB S.A.</option>
              <option value="757">757 Banco KEB HANA do Brasil S.A.</option>
              <option value="600">600 Banco Luso Brasileiro S.A.</option>
              <option value="389">389 Banco Mercantil do Brasil S.A.</option>
              <option value="370">370 Banco Mizuho do Brasil S.A.</option>
              <option value="746">746 Banco Modal S.A.</option>
              <option value="66">66 Banco Morgan Stanley S.A.</option>
              <option value="456">456 Banco MUFG Brasil S.A.</option>
              <option value="243">243 Banco Máxima S.A.</option>
              <option value="7">7 Banco Nacional de Desenvolvimento Econômico e Social - BNDES</option>
              <option value="169">169 Banco Olé Bonsucesso Consignado S.A.</option>
              <option value="79">79 Banco Original do Agronegócio S.A.</option>
              <option value="212">212 Banco Original S.A.</option>
              <option value="712">712 Banco Ourinvest S.A.</option>
              <option value="623">623 Banco PAN S.A.</option>
              <option value="611">611 Banco Paulista S.A.</option>
              <option value="643">643 Banco Pine S.A.</option>
              <option value="747">747 Banco Rabobank International Brasil S.A.</option>
              <option value="633">633 Banco Rendimento S.A.</option>
              <option value="741">741 Banco Ribeirão Preto S.A.</option>
              <option value="120">120 Banco Rodobens S.A.</option>
              <option value="422">422 Banco Safra S.A.</option>
              <option value="33">33 Banco Santander (Brasil) S.A.</option>
              <option value="743">743 Banco Semear S.A.</option>
              <option value="754">754 Banco Sistema S.A.</option>
              <option value="630">630 Banco Smartbank S.A.</option>
              <option value="366">366 Banco Société Générale Brasil S.A.</option>
              <option value="637">637 Banco Sofisa S.A.</option>
              <option value="464">464 Banco Sumitomo Mitsui Brasileiro S.A.</option>
              <option value="82">82 Banco Topázio S.A.</option>
              <option value="18">18 Banco Tricury S.A.</option>
              <option value="634">634 Banco Triângulo S.A.</option>
              <option value="655">655 Banco Votorantim S.A.</option>
              <option value="610">610 Banco VR S.A.</option>
              <option value="119">119 Banco Western Union do Brasil S.A.</option>
              <option value="124">124 Banco Woori Bank do Brasil S.A.</option>
              <option value="348">348 Banco XP S.A.</option>
              <option value="81">81 BancoSeguro S.A.</option>
              <option value="21">21 BANESTES S.A. Banco do Estado do Espírito Santo</option>
              <option value="755">755 Bank of America Merrill Lynch Banco Múltiplo S.A.</option>
              <option value="268">268 BARI COMPANHIA HIPOTECÁRIA</option>
              <option value="250">250 BCV - Banco de Crédito e Varejo S.A.</option>
              <option value="144">144 BEXS Banco de Câmbio S.A.</option>
              <option value="253">253 Bexs Corretora de Câmbio S/A</option>
              <option value="134">134 BGC LIQUIDEZ DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS LTDA
              </option>
              <option value="17">17 BNY Mellon Banco S.A.</option>
              <option value="301">301 BPP Instituição de Pagamento S.A.</option>
              <option value="126">126 BR Partners Banco de Investimento S.A.</option>
              <option value="70">70 BRB - Banco de Brasília S.A.</option>
              <option value="92">92 Brickell S.A. Crédito</option>
              <option value="173">173 BRL Trust Distribuidora de Títulos e Valores Mobiliários S.A.
              </option>
              <option value="142">142 Broker Brasil Corretora de Câmbio Ltda.</option>
              <option value="292">292 BS2 Distribuidora de Títulos e Valores Mobiliários S.A.</option>
              <option value="104">104 Caixa Econômica Federal</option>
              <option value="309">309 CAMBIONET CORRETORA DE CÂMBIO LTDA.</option>
              <option value="288">288 CAROL DISTRIBUIDORA DE TITULOS E VALORES MOBILIARIOS LTDA.</option>
              <option value="130">130 CARUANA S.A. - SOCIEDADE DE CRÉDITO, FINANCIAMENTO E INVESTIMENTO
              </option>
              <option value="159">159 Casa do Crédito S.A. Sociedade de Crédito ao Microempreendedor
              </option>
              <option value="114">114 Central Cooperativa de Crédito no Estado do Espírito Santo -
                CECOOP
              </option>
              <option value="91">91 CENTRAL DE COOPERATIVAS DE ECONOMIA E CRÉDITO MÚTUO DO ESTADO DO RIO
                GRANDE DO S
              </option>
              <option value="320">320 China Construction Bank (Brasil) Banco Múltiplo S.A.</option>
              <option value="477">477 Citibank N.A.</option>
              <option value="180">180 CM CAPITAL MARKETS CORRETORA DE CÂMBIO, TÍTULOS E VALORES
                MOBILIÁRIOS LTDA
              </option>
              <option value="127">127 Codepe Corretora de Valores e Câmbio S.A.</option>
              <option value="163">163 Commerzbank Brasil S.A. - Banco Múltiplo</option>
              <option value="133">133 CONFEDERAÇÃO NACIONAL DAS COOPERATIVAS CENTRAIS DE CRÉDITO E
                ECONOMIA FAMILIAR E
              </option>
              <option value="136">136 CONFEDERAÇÃO NACIONAL DAS COOPERATIVAS CENTRAIS UNICRED LTDA. -
                UNICRED DO BRASI
              </option>
              <option value="60">60 Confidence Corretora de Câmbio S.A.</option>
              <option value="85">85 Cooperativa Central de Crédito - AILOS</option>
              <option value="97">97 Cooperativa Central de Crédito Noroeste Brasileiro Ltda.</option>
              <option value="279">279 COOPERATIVA DE CREDITO RURAL DE PRIMAVERA DO LESTE</option>
              <option value="16">16 COOPERATIVA DE CRÉDITO MÚTUO DOS DESPACHANTES DE TRÂNSITO DE SANTA
                CATARINA E RI
              </option>
              <option value="281">281 Cooperativa de Crédito Rural Coopavel</option>
              <option value="322">322 Cooperativa de Crédito Rural de Abelardo Luz - Sulcredi/Crediluz
              </option>
              <option value="286">286 COOPERATIVA DE CRÉDITO RURAL DE OURO SULCREDI/OURO</option>
              <option value="273">273 Cooperativa de Crédito Rural de São Miguel do Oeste - Sulcredi/São
                Miguel
              </option>
              <option value="98">98 Credialiança Cooperativa de Crédito Rural</option>
              <option value="10">10 CREDICOAMO CREDITO RURAL COOPERATIVA</option>
              <option value="89">89 CREDISAN COOPERATIVA DE CRÉDITO</option>
              <option value="11">11 CREDIT SUISSE HEDGING-GRIFFO CORRETORA DE VALORES S.A</option>
              <option value="342">342 Creditas Sociedade de Crédito Direto S.A.</option>
              <option value="321">321 CREFAZ SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E A EMPRESA DE
                PEQUENO PORTE LT
              </option>
              <option value="289">289 DECYSEO CORRETORA DE CAMBIO LTDA.</option>
              <option value="487">487 Deutsche Bank S.A. - Banco Alemão</option>
              <option value="140">140 Easynvest - Título Corretora de Valores SA</option>
              <option value="149">149 Facta Financeira S.A. - Crédito Financiamento e Investimento
              </option>
              <option value="196">196 FAIR CORRETORA DE CAMBIO S.A.</option>
              <option value="343">343 FFA SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E À EMPRESA DE PEQUENO
                PORTE LTDA.
              </option>
              <option value="331">331 Fram Capital Distribuidora de Títulos e Valores Mobiliários S.A.
              </option>
              <option value="285">285 Frente Corretora de Câmbio Ltda.</option>
              <option value="278">278 Genial Investimentos Corretora de Valores Mobiliários S.A.</option>
              <option value="364">364 GERENCIANET PAGAMENTOS DO BRASIL LTDA</option>
              <option value="138">138 Get Money Corretora de Câmbio S.A.</option>
              <option value="64">64 Goldman Sachs do Brasil Banco Múltiplo S.A.</option>
              <option value="177">177 Guide Investimentos S.A. Corretora de Valores</option>
              <option value="146">146 GUITTA CORRETORA DE CAMBIO LTDA.</option>
              <option value="78">78 Haitong Banco de Investimento do Brasil S.A.</option>
              <option value="62">62 Hipercard Banco Múltiplo S.A.</option>
              <option value="189">189 HS FINANCEIRA S/A CREDITO, FINANCIAMENTO E INVESTIMENTOS</option>
              <option value="269">269 HSBC Brasil S.A. - Banco de Investimento</option>
              <option value="271">271 IB Corretora de Câmbio, Títulos e Valores Mobiliários S.A.</option>
              <option value="157">157 ICAP do Brasil Corretora de Títulos e Valores Mobiliários Ltda.
              </option>
              <option value="132">132 ICBC do Brasil Banco Múltiplo S.A.</option>
              <option value="492">492 ING Bank N.V.</option>
              <option value="139">139 Intesa Sanpaolo Brasil S.A. - Banco Múltiplo</option>
              <option value="652">652 Itaú Unibanco Holding S.A.</option>
              <option value="341">341 Itaú Unibanco S.A.</option>
              <option value="488">488 JPMorgan Chase Bank</option>
              <option value="399">399 Kirton Bank S.A. - Banco Múltiplo</option>
              <option value="293">293 Lastro RDV Distribuidora de Títulos e Valores Mobiliários Ltda.
              </option>
              <option value="105">105 Lecca Crédito, Financiamento e Investimento S/A</option>
              <option value="145">145 LEVYCAM - CORRETORA DE CAMBIO E VALORES LTDA.</option>
              <option value="113">113 Magliano S.A. Corretora de Cambio e Valores Mobiliarios</option>
              <option value="323">323 MERCADOPAGO.COM REPRESENTACOES LTDA.</option>
              <option value="274">274 MONEY PLUS SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E A EMPRESA DE
                PEQUENO PORT
              </option>
              <option value="259">259 MONEYCORP BANCO DE CÂMBIO S.A.</option>
              <option value="128">128 MS Bank S.A. Banco de Câmbio</option>
              <option value="354">354 NECTON INVESTIMENTOS S.A. CORRETORA DE VALORES MOBILIÁRIOS E
                COMMODITIES
              </option>
              <option value="191">191 Nova Futura Corretora de Títulos e Valores Mobiliários Ltda.
              </option>
              <option value="753">753 Novo Banco Continental S.A. - Banco Múltiplo</option>
              <option value="260">260 Nu Pagamentos S.A.</option>
              <option value="111">111 OLIVEIRA TRUST DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIARIOS S.A.
              </option>
              <option value="319">319 OM DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS LTDA</option>
              <option value="613">613 Omni Banco S.A.</option>
              <option value="290">290 Pagseguro Internet S.A.</option>
              <option value="254">254 Paraná Banco S.A.</option>
              <option value="326">326 PARATI - CREDITO, FINANCIAMENTO E INVESTIMENTO S.A.</option>
              <option value="194">194 PARMETAL DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS LTDA
              </option>
              <option value="174">174 PERNAMBUCANAS FINANCIADORA S.A. - CRÉDITO, FINANCIAMENTO E
                INVESTIMENTO
              </option>
              <option value="315">315 PI Distribuidora de Títulos e Valores Mobiliários S.A.</option>
              <option value="100">100 Planner Corretora de Valores S.A.</option>
              <option value="125">125 Plural S.A. - Banco Múltiplo</option>
              <option value="108">108 PORTOCRED S.A. - CREDITO, FINANCIAMENTO E INVESTIMENTO</option>
              <option value="306">306 PORTOPAR DISTRIBUIDORA DE TITULOS E VALORES MOBILIARIOS LTDA.
              </option>
              <option value="93">93 PÓLOCRED SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E À EMPRESA DE
                PEQUENO PORT
              </option>
              <option value="329">329 QI Sociedade de Crédito Direto S.A.</option>
              <option value="283">283 RB CAPITAL INVESTIMENTOS DISTRIBUIDORA DE TÍTULOS E VALORES
                MOBILIÁRIOS LIMITADA
              </option>
              <option value="101">101 RENASCENCA DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS LTDA
              </option>
              <option value="270">270 Sagitur Corretora de Câmbio Ltda.</option>
              <option value="751">751 Scotiabank Brasil S.A. Banco Múltiplo</option>
              <option value="276">276 Senff S.A. - Crédito, Financiamento e Investimento</option>
              <option value="545">545 SENSO CORRETORA DE CAMBIO E VALORES MOBILIARIOS S.A</option>
              <option value="190">190 SERVICOOP - COOPERATIVA DE CRÉDITO DOS SERVIDORES PÚBLICOS ESTADUAIS
                DO RIO GRAN
              </option>
              <option value="183">183 SOCRED S.A. - SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E À EMPRESA
                DE PEQUENO P
              </option>
              <option value="365">365 SOLIDUS S.A. CORRETORA DE CAMBIO E VALORES MOBILIARIOS</option>
              <option value="299">299 SOROCRED CRÉDITO, FINANCIAMENTO E INVESTIMENTO S.A.</option>
              <option value="14">14 State Street Brasil S.A. - Banco Comercial</option>
              <option value="197">197 Stone Pagamentos S.A.</option>
              <option value="340">340 Super Pagamentos e Administração de Meios Eletrônicos S.A.</option>
              <option value="307">307 Terra Investimentos Distribuidora de Títulos e Valores Mobiliários
                Ltda.
              </option>
              <option value="352">352 TORO CORRETORA DE TÍTULOS E VALORES MOBILIÁRIOS LTDA</option>
              <option value="95">95 Travelex Banco de Câmbio S.A.</option>
              <option value="143">143 Treviso Corretora de Câmbio S.A.</option>
              <option value="131">131 TULLETT PREBON BRASIL CORRETORA DE VALORES E CÂMBIO LTDA</option>
              <option value="129">129 UBS Brasil Banco de Investimento S.A.</option>
              <option value="15">15 UBS Brasil Corretora de Câmbio, Títulos e Valores Mobiliários S.A.
              </option>
              <option value="99">99 UNIPRIME CENTRAL - CENTRAL INTERESTADUAL DE COOPERATIVAS DE CREDITO
                LTDA.
              </option>
              <option value="84">84 Uniprime Norte do Paraná - Coop de Economia e Crédito Mútuo dos
                Médicos
              </option>
              <option value="373">373 UP.P SOCIEDADE DE EMPRÉSTIMO ENTRE PESSOAS S.A.</option>
              <option value="298">298 Vip's Corretora de Câmbio Ltda.</option>
              <option value="296">296 VISION S.A. CORRETORA DE CAMBIO</option>
              <option value="367">367 VITREO DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS S.A.</option>
              <option value="310">310 VORTX DISTRIBUIDORA DE TITULOS E VALORES MOBILIARIOS LTDA.</option>
              <option value="102">102 XP INVESTIMENTOS CORRETORA DE CÂMBIO,TÍTULOS E VALORES MOBILIÁRIOS
                S/A
              </option>
              <option value="325">325 Órama Distribuidora de Títulos e Valores Mobiliários S.A.</option>
              <option value="355">355 ÓTIMO SOCIEDADE DE CRÉDITO DIRETO S.A.</option>

            </select>
          </div>
        </div>
      </div>
      <div class="row">

        <div class="col-md-3">
          <div class="form-group">
            <label>Agência</label>
            <input type="text" name="agencia" id="agencia" class="form-control" required>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Conta</label>
            <input type="text" name="conta" id="conta" class="form-control" required>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Operação</label>
            <select name="operacao" class="form-control" required>
              <option value="">Selecione</option>
              <option value="c">Corrente</option>
              <option value="p">Poupança</option>
            </select>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>CPF do Titular</label>
            <input type="text" name="cpfTitularConta" class="form-control" required>
          </div>
        </div>
      </div>


      <div class="row">

        <div class="col-md-12 text-center" style="margin-top: 20px;">
          <span style="color: red">
            Ao continuar, declaro, para os devidos fins, que sou inteiramente responsável pela validade e vericidade dos dados de cadastro fornecidos.
          </span>
        </div>
      </div>
    </div>

    <h4>Resumo</h4>
    <hr>

    <!--        <div class="listaCertificados"></div>-->
    <div class="dadosUsuario">
      <div class="row">
        <div class="col-sm-12 col-lg-3 text-center">
          <strong>Nome: </strong> <span id="dadosUsuarioNome"></span>
        </div>
        <div class="col-sm-12 col-lg-3 text-center">
          <strong>Email: </strong> <span id="dadosUsuarioEmail"></span>
        </div>
        <div class="col-sm-12 col-lg-3 text-center">
          <strong>Quantidade de Certificados: </strong> <span id="dadosUsuarioQtd"></span>
        </div>
        <div class="col-sm-12 col-lg-3 text-center">
          <strong>Valor Total: </strong> <span id="dadosUsuarioTotal"></span>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="alert alert-danger" id="alerta-error" role="alert" style="display: none; margin-top:15px;">
          <span id="alerta-error-msg"></span>
          <button type="button" class="close" onclick="fecharAlerta()">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div>
    </div>


    <div class="row" style="margin-top: 20px;">
      <div class="col"></div>
      <div class="col-md-3">
        <button id="btnFinalizarEscolher" class="btn btn-success btn-block text-uppercase" type="button"
                style="display: none">
          <i class="fas fa-check"></i> Confirmar
        </button>
        <button id="btnFinalizarDiretoPagSeguro" class="btn btn-success btn-block text-uppercase" type="button"
                style="display: none">
          <i class="fas fa-check"></i> Confirmar
        </button>
        <button id="btnFinalizarDiretoPagSorte" class="btn btn-success btn-block text-uppercase" type="button"
                style="display: none">
          <i class="fas fa-check"></i> Confirmar
        </button>
      </div>
      <div class="col"></div>
    </div>
  </form>
</div>

<div id="conteudo-selecionar" class="container py-5" data-aos="zoom-in" style="display: none">
  <div id="selecionar-pagamento">
    <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
      <div class="col-md-12 text-center">
        <h3 class="mt-20 mb-20">Selecione o método de pagamento:</h3>
      </div>
    </div>
    <div class="row gateways">
      <div class="col-md-4"></div>
      <div class="col-md-4">
        <div class="card cursor-pointer border-success gateway d-none" id="gateway-pagseguro"
             onclick="selecionarPagamento('pagseguro')">
          <div class="card-body text-center">
            <img src="images/pagseguro_logo.png" alt="PagSeguro">
          </div>
        </div>
        <div class="card cursor-pointer border-success gateway d-none" id="gateway-picpay"
             onclick="selecionarPagamento('picpay')">
          <div class="card-body text-center">
            <img src="images/picpay-logo.png" alt="Picpay">
          </div>
        </div>
        <div class="card cursor-pointer border-juno gateway d-none" id="gateway-juno"
             onclick="selecionarPagamento('juno')">
          <div class="card-body text-center">
            <img src="images/logo-juno.jpg" alt="Juno">
          </div>
        </div>
        <div class="card cursor-pointer border-info gateway d-none" id="gateway-aixmobil"
             onclick="selecionarPagamento('pagsorte')">
          <div class="card-body text-center">
            <img src="images/aixmobil.png" alt="Aixmobil">
          </div>
        </div>
      </div>
      <div class="col-md-4"></div>
    </div>
  </div>
</div>

<div id="conteudo-pagamento" style="display: none;">
  <div class="contentAll">
    <div class="container py-4 main">
      <form action="#" id="form-pagamento" role="form" data-toggle="validator" method="post"
            accept-charset="utf-8"
            data-encrypted-form>
        <div id="form-step-2" role="form" data-toggle="validator">
          <div class="general h-auto mb-auto mt-auto d-fex justify-content-center align-items-center">
            <div class="request-card active" data-show='0'>
              <h2 class="text-center mb-4" style="margin-bottom: 0 !important;">Pagamento</h2>
              <!--            <div class="row">-->
              <!--              <div class="col col-md-6">-->
              <!--              </div>-->
              <!--              <div class="col col-md-6 text-right">-->
              <!--                <br>-->
              <!--                <button-->
              <!--                    class="btn btn-default mx-auto w-auto text-center btn-valida-lista-cartoes mb-5 rounded border-secondary"-->
              <!--                    onclick="mostrarCartoesCadastrados()">-->
              <!--                  <i class="fa fa-plus-circle" aria-hidden="true"></i> Cartões cadastrados-->
              <!--                </button>-->
              <!--              </div>-->
              <!--            </div>-->
              <div class="card-wrapper py-3"></div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Forma de Pagamento:</label>
                    <div class="row">
                      <div class="col">
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="forma_pagamento"
                                 id="forma_pagamentoC"
                                 value="1" checked required>
                          <label class="form-check-label" for="forma_pagamentoC">Cartão de
                            crédito</label>
                        </div>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="forma_pagamento"
                                 id="forma_pagamentoD"
                                 value="0" required>
                          <label class="form-check-label" for="forma_pagamentoD">Cartão de
                            débito</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!--                <div class="form-group col-xs-12 col-md-12">-->
                <!--                  <div><label for="nome">Forma de Pagamento:</label></div>-->
                <!--                  <div>-->
                <!--                    <label class="pr-3" style="cursor:pointer">-->
                <!--                      <input type="radio" class="d-none forma_pagamento" name="forma_pagamento"-->
                <!--                             onclick="alterarMetodoPagamento(this)" value="1" required checked>-->
                <!--                      <i class="fa fa-fw fa-check-circle-o"></i>&nbsp; Cartão de crédito-->
                <!--                    </label>-->
                <!---->
                <!--                    <label style="cursor:pointer" ;>-->
                <!--                      <input type="radio" class="d-none forma_pagamento" name="forma_pagamento"-->
                <!--                             onclick="alterarMetodoPagamento(this)" value="0" required>-->
                <!--                      <i class="fa fa-fw fa-check-circle-o"></i>&nbsp;Débito online-->
                <!--                    </label>-->
                <!--                  </div>-->
                <!--                </div>-->
                <div class="form-group col-xs-12 col-md-4">
                  <label for="nome">E-mail:
                    <div style="display: inline;">
                      <span style="color:red"> *</span>
                    </div>
                  </label>
                  <input type="email" name="email" class="form-control" data-campo="Email" id="email"
                         required>
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  <label for="nome">CPF:
                    <div style="display: inline;">
                      <span style="color:red"> *</span>
                    </div>
                  </label>
                  <input type="text" class="form-control" id="cpf" data-campo="Cpf" required>
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  <label for="nome">Telefone:</label>
                  <input type="text" class="form-control" id="telefone" data-campo="Telefone"
                         inputmode="numeric">
                </div>
                <!--              <div class="col-md-12 col-xs-12 general-save-card">-->
                <!--                <div id="checkbox-content" class="form-group d-flex align-items-center gp-save-card">-->
                <!--                  <input type="checkbox" class="d-none" name="save-card" id="save-card" autocomplete="off"/>-->
                <!--                  <i class="fa fa-check-square-o save-card" aria-hidden="true"></i>-->
                <!--                  <i class="fa fa-square-o save-card active" aria-hidden="true"></i>-->
                <!--                  <div class="[ btn-group ]">-->
                <!--                    <label for="save-card" class="btn btn-default m-0 btn-save-card text-left">-->
                <!--                      <b> Salvar cartão para próximas compras?</b>-->
                <!--                    </label>-->
                <!--                  </div>-->
                <!--                </div>-->
                <!--              </div>-->
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="nome">Número do cartão:
                      <div style="display: inline;">
                        <span style="color:red"> *</span>
                      </div>
                    </label>
                    <input type="text" class="form-control credit-card" id="numero_cartao"
                           data-campo="Números do cartão"
                           maxlength="19" onkeypress="return onlynumber();"
                           onblur="valida_bandeira(this)"
                           inputmode="numeric" required>
                    <div id="card-img" class="text-center">
                    <span class="spinner-border text-primary spinner-border-sm d-none" role="status"
                          aria-hidden="true"></span>
                      <span id="img"></span>
                    </div>
                    <div class="invalid-feedback">
                      Cartão inválido ou número incompleto
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="nome">Nome do titular do cartão:
                      <div style="display: inline;">
                        <span style="color:red"> *</span>
                      </div>
                    </label>
                    <input type="text" class="form-control" id="nome_titular"
                           data-campo="Titular do Cartão" required>
                  </div>
                </div>
                <div class="col-md-4 col-xs-12">
                  <div class="form-group">
                    <label for="mes-validade">Mês da Validade: <span style="color:red"> *</span>
                      <span style="font-size: 12px; display: inline;">Ex:
                      <span style="color:red"> 01</span>
                      </span>
                    </label>
                    <input type="hidden" id="validade">
                    <select type="text" class="form-control" id="mes-validade"
                            data-campo="Mês de Validade do Cartão"
                            required>
                      <option value="">Mês da Validade</option>
                      <option value="01">01</option>
                      <option value="02">02</option>
                      <option value="03">03</option>
                      <option value="04">04</option>
                      <option value="05">05</option>
                      <option value="06">06</option>
                      <option value="07">07</option>
                      <option value="08">08</option>
                      <option value="09">09</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-4 col-xs-12">
                  <div class="form-group">
                    <label for="ano-validade">Ano da Validade:
                      <span style="color:red"> *</span>
                      <span style="font-size: 12px; display: inline;">Ex:
                      <span style="color:red"> 2023</span>
                      </span>
                    </label>
                    <select type="text" class="form-control" id="ano-validade"
                            data-campo="Ano de Validade do Cartão"
                            required>
                      <option value="">Ano da Validade</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                      <option value="2022">2022</option>
                      <option value="2023">2023</option>
                      <option value="2024">2024</option>
                      <option value="2025">2025</option>
                      <option value="2026">2026</option>
                      <option value="2027">2027</option>
                      <option value="2028">2028</option>
                      <option value="2029">2029</option>
                      <option value="2030">2030</option>
                      <option value="2031">2031</option>
                      <option value="2032">2032</option>
                      <option value="2033">2033</option>
                      <option value="2034">2034</option>
                      <option value="2035">2035</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-4 col-xs-12">
                  <div class="form-group">
                    <label for="nome">CVV:
                      <span style="color:red"> *</span>
                      <a href="javascript:void(0)" class="link-info" title="Onde encontrar o CVV"
                         onclick="mostrarAjudaCVV()"><i
                            class="fa fa-question-circle"></i></a></label>
                    <input type="number" class="form-control" data-campo="Código CCV do Cartão"
                           id="codigo" required>
                  </div>
                </div>
                <!--                <div class="col-md-4 col-xs-12">-->
                <!--                    <div class="form-group">-->
                <!--                        <label for="hastag-da-sorte-newcard">Apresentador</label>-->
                <!--                        <input type="text" id="hastag-da-sorte-newcard" class="hashtag form-control hastag-da-sorte-newcard">-->
                <!--                    </div>-->
                <!--                </div>-->
                <!--                <div class="col-md-4 col-xs-12 text-center">-->
                <!--                  <a href="#" class="btn btn-light mx-auto w-auto text-center d-block W-100" onclick="voltarTela()"-->
                <!--                     style="width: 100% !important; margin-top: 7px">Voltar</a>-->
                <!--                </div>-->
                <div class="col-md-8 col-xs-12" id="valor" style="display: none">
                  <h4 style="color: #205177; border: 1px solid #ccc; border-radius: 4px; padding: 7px 7px 5px; margin: 7px 0 0 !important; line-height: 17px; text-align: center;">
                    Total a pagar: <label id="dados-valor"></label></h4>
                </div>
                <div class="col col-md-4">
                  <!--                <div><label for="nome"></label></div>-->
                  <button class="btn btn-success mx-auto w-auto text-center d-block W-100"
                          id="assinar"
                          style="width: 100% !important; margin-top: 7px">
                                        <span class="spinner-border spinner-border-sm d-none" role="status"
                                              aria-hidden="true"></span>
                    <span id="text" class="d-block"> Efetuar compra</span>
                  </button>
                </div>
              </div>
              <div class="row" style="margin-top: 70px">
                <div class="col-md-12 text-center">
                  <img id="trustwaveSealImage"
                       src="https://sealserver.trustwave.com/seal_image.php?customerId=5cb287c9dda14d64b14c29d3257b8c2a&amp;size=105x54&amp;style=invert"
                       border="0" style="cursor:pointer;"
                       onclick="javascript:window.open('https://sealserver.trustwave.com/cert.php?customerId=5cb287c9dda14d64b14c29d3257b8c2a&amp;size=105x54&amp;style=invert', 'c_TW', 'location=no, toolbar=no, resizable=yes, scrollbars=yes, directories=no, status=no, width=615, height=720'); return false;"
                       oncontextmenu="javascript:alert('Copying Prohibited by Law - Trusted Commerce is a Service Mark of TrustWave Holdings, Inc.'); return false;"
                       alt="This site is protected by Trustwave's Trusted Commerce program"
                       title="This site is protected by Trustwave's Trusted Commerce program">
                </div>
              </div>
              <input type="hidden" id="bandeira">
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="info-concluido d-none justify-content-center align-items-center  vh-100">
    <div style="border: 2px solid #000; border-radius: 20px; background: #f0f0f0;" class="p-5">
      <div class="col-sm-12">
        <h2 class="text-uppercase d-flex justify-content-center align-items-center text-center"
            style="color:#2ecc71">
          <i class="fa fa-check-circle-o fa-2x pr-2" aria-hidden="true" style="color:#27ae60"></i>
          <b>Finalizada com sucesso!</b>
        </h2>
        <h3 class="text-center text-uppercase">Boa sorte!</h3>
        <h4 class="text-center text-uppercase">Certificado(s) enviado(s) por email e no menu "Meus
          Certificados"</h4>
      </div>
    </div>
  </div>
  <div class="info-rejeitado d-none justify-content-center align-items-center  vh-100">
    <div style="border: 2px solid #000; border-radius: 20px; background: #f0f0f0;" class="p-5">
      <div class="col-sm-12">
        <h2 class="text-uppercase d-flex justify-content-center align-items-center text-center "
            style="color:red">
          <i class="fa fa-times-circle  fa-2x pr-2" aria-hidden="true" style="color:red"></i>
          <b>Ocorreu um erro!</b>
        </h2>
        <h3 class="text-center text-uppercase">Tente novamente!</h3>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal-ajuda-cvv" tabindex="-1" role="dialog" aria-labelledby="modal-ajuda-cvv"
       aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalScrollableTitle">Ajuda - Onde encontrar o CVV</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <img src="images/what_cvv.jpg" style="max-width: 100%;" alt="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="conteudo-pagamento-juno" style="display: none;">
  <div class="contentAll">
    <div class="container py-4 main ">
      <form id="form-cadastro-juno" class="vh-100" data-toggle="validator" method="post" accept-charset="utf-8">
        <div class="row d-sm-block d-md-flex">
          <div class="col-md-6 ">
            <h3 class="mb-5">
              Dados do Cliente
            </h3>


            <div class="row">
              <div class="form-group col-xs-12 col-md-6">
                <label for="nome">Seu nome:
                  <div style="display: inline;">
                    <span style="color:red"> *</span>
                  </div>
                </label>
                <input type="text" name="nome" class="form-control" id="juno-nome" data-campo="Nome" required>
              </div>
              <div class="form-group col-xs-12 col-md-6">
                <label for="nome">Sobrenome:
                  <div style="display: inline;">
                    <span style="color:red"> *</span>
                  </div>
                </label>
                <input type="text" name="sobrenome" class="form-control" data-campo="Sobrenome" id="juno-sobrenome"
                       required>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-xs-12 col-md-4">
                <label for="nome">E-mail:
                  <div style="display: inline;">
                    <span style="color:red"> *</span>
                  </div>
                </label>
                <input type="email" name="email" class="form-control"  data-campo="Email" id="juno-email" required>
              </div>
              <div class="form-group col-xs-12 col-md-4">
                <label for="nome">
                  Telefone:
                  <div style="display: inline;">
                    <span style="color:red"> *</span>
                  </div>
                </label>
                <input type="text" class="form-control mask-telefone" id="juno-telefone" data-campo="Telefone"
                       required
                       inputmode="numeric">
              </div>
              <div class="form-group col-xs-12 col-md-4">
                <label for="nome">CPF:
                  <div style="display: inline;">
                    <span style="color:red"> *</span>
                  </div>
                </label>
                <input type="text" class="form-control mask-cpf" id="juno-cpf" name="juno-cpf" data-campo="Cpf" required>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-xs-12 col-md-8">
                <label for="nome">Endereço:
                  <div style="display: inline;">
                    <span style="color:red"> *</span>
                  </div>
                </label>
                <input type="text" class="form-control" id="juno-endereco" name="juno-endereco" data-campo="Endereço" required>
              </div>
              <div class="form-group col-xs-12 col-md-4">
                <label for="juno-numero">Número:
                  <span style="color:red"> *</span>
                </label>
                <input type="text" class="form-control" id="juno-numero" name="juno-numero" data-campo="Número" required>
              </div>

            </div>
            <div class="row">

              <div class="form-group col-xs-12 col-md-6">
                <label for="juno-complemento">Complemento:
                </label>
                <input type="text" class="form-control" id="juno-complemento" name="juno-complemento" data-campo="Complemento">
              </div>

              <div class="form-group col-xs-12 col-md-6">
                <label for="nome">Bairro:
                  <div style="display: inline;">
                    <span style="color:red"> *</span>
                  </div>
                </label>
                <input type="text" class="form-control" id="juno-bairro" name="juno-bairro" data-campo="Bairro" required>
              </div>
              <div class="form-group col-xs-12 col-md-6">
                <label for="nome">Cidade:
                  <div style="display: inline;">
                    <span style="color:red"> *</span>
                  </div>
                </label>
                <input type="text" class="form-control" id="juno-cidade" name="juno-cidade" data-campo="Cidade" required>
              </div>
              <div class="form-group col-xs-12 col-md-6">
                <label for="nome">Estado:
                  <div style="display: inline;">
                    <span style="color:red"> *</span>
                  </div>
                </label>
                <select class="form-control" id="juno-estado" data-campo="Estado" name="juno-estado" required>
                  <option value="" selected disabled>Selecione</option>
                  <option value="AC">Acre</option>
                  <option value="AL">Alagoas</option>
                  <option value="AP">Amapá</option>
                  <option value="AM">Amazonas</option>
                  <option value="BA">Bahia</option>
                  <option value="CE">Ceará</option>
                  <option value="DF">Distrito Federal</option>
                  <option value="ES">Espírito Santo</option>
                  <option value="GO">Goiás</option>
                  <option value="MA">Maranhão</option>
                  <option value="MT">Mato Grosso</option>
                  <option value="MS">Mato Grosso do Sul</option>
                  <option value="MG">Minas Gerais</option>
                  <option value="PA">Pará</option>
                  <option value="PB">Paraíba</option>
                  <option value="PR">Paraná</option>
                  <option value="PE">Pernambuco</option>
                  <option value="PI">Piauí</option>
                  <option value="RJ">Rio de Janeiro</option>
                  <option value="RN">Rio Grande do Norte</option>
                  <option value="RS">Rio Grande do Sul</option>
                  <option value="RO">Rondônia</option>
                  <option value="RR">Roraima</option>
                  <option value="SC">Santa Catarina</option>
                  <option value="SP">São Paulo</option>
                  <option value="SE">Sergipe</option>
                  <option value="TO">Tocantins</option>
                </select>
              </div>
              <div class="form-group col-xs-12 col-md-6">
                <label for="juno-pais">País:
                  <span style="color:red"> *</span>
                </label>
                <select name="pais" id="juno-pais" class="form-control" data-campo="País" required>
                  <option value="">Selecione</option>
                  <option value="BRA" selected>Brasil</option>
                </select>
              </div>
              <div class="form-group col-xs-12 col-md-6">
                <label for="nome">CEP:
                  <span style="color:red"> *</span>
                </label>
                <input type="text" class="form-control mask-cep" id="juno-cep" name="juno-cep" data-campo="CEP" required>
              </div>
            </div>

          </div>
          <div class="col-md-6">
            <h3 class="mb-3">
              Dados de pagamento
            </h3>

            <div class="general h-auto mb-auto mt-auto d-fex justify-content-center align-items-center">
              <div class="request-card active" data-show='0'>
                <div class="card-wrapper-juno py-3"></div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                      <label for="nome">Número do cartão:</label>
                      <input type="text" class="form-control credit-card mask-numcartao" id="juno-numero_cartao" name="juno-numero_cartao"
                             data-campo="Números do cartão" maxlength="19" onkeypress="return onlynumber();"
                             inputmode="numeric">
                      <div id="card-img" class="text-center">
                            <span class="spinner-border text-primary spinner-border-sm d-none" role="status"
                                  aria-hidden="true"></span>
                        <span id="img"></span>
                      </div>
                      <div class="invalid-feedback">
                        Cartão inválido ou número incompleto
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                      <label for="nome">Nome do titular do cartão:</label>
                      <input type="text" class="form-control" id="juno-nome_titular" name="juno-nome_titular" data-campo="Titular do Cartão"
                             required>
                    </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                      <label for="mes-validade">Mês da Validade: <span style="color:red"> *</span>
                        <span style="font-size: 12px; display: inline;">Ex:
                                                        <span style="color:red"> 01</span>
                                                    </span>
                      </label>
                      <input type="hidden" id="validade">
                      <select type="text" class="form-control" id="juno-mes-validade" name="juno-mes-validade"
                              data-campo="Mês de Validade do Cartão"
                              required>
                        <option value="">Mês da Validade</option>
                        <option value="01">01</option>
                        <option value="02">02</option>
                        <option value="03">03</option>
                        <option value="04">04</option>
                        <option value="05">05</option>
                        <option value="06">06</option>
                        <option value="07">07</option>
                        <option value="08">08</option>
                        <option value="09">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                      <label for="ano-validade">Ano da Validade:
                        <span style="color:red"> *</span>
                        <span style="font-size: 12px; display: inline;">Ex:
                                                        <span style="color:red"> 2023</span>
                                                        </span>
                      </label>
                      <select type="text" class="form-control" id="juno-ano-validade" name="juno-ano-validade"
                              data-campo="Ano de Validade do Cartão"
                              required>
                        <option value="">Ano da Validade</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                        <option value="2026">2026</option>
                        <option value="2027">2027</option>
                        <option value="2028">2028</option>
                        <option value="2029">2029</option>
                        <option value="2030">2030</option>
                        <option value="2031">2031</option>
                        <option value="2032">2032</option>
                        <option value="2033">2033</option>
                        <option value="2034">2034</option>
                        <option value="2035">2035</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                      <label for="nome">CVV:
                        <span style="color:red"> *</span>
                        <a href="javascript:void(0)" class="link-info" title="Onde encontrar o CVV"
                           onclick="mostrarAjudaCVVJuno()"><i
                              class="fa fa-question-circle"></i></a></label>
                      <input type="number" class="form-control mask-cvv" data-campo="Código CCV do Cartão" name="juno-codigo"
                             id="juno-codigo"
                             required>
                    </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                      <label for="nome">Data de Nasc.:</label>
                      <input type="text" class="form-control mask-data_nascimento" id="juno-data_nascimento" name="juno-data_nascimento">
                    </div>
                  </div>
                </div>

                <input type="hidden" id="bandeira" value="">
                <input type="hidden" id="cod_plano" value="">
                <input type="hidden" id="hash_user" value="">
              </div>
            </div>
          </div>
        </div>
        <div class="row" style="margin-top: 20px;">
          <div class="col-md-2"></div>
          <div class="col-md-8 col-xs-12" id="valor-juno" style="display: none">
            <h4 style="color: #205177; border: 1px solid #ccc; border-radius: 4px; padding: 7px 7px 5px; margin: 7px 0 0 !important; line-height: 17px; text-align: center;">
              Total a pagar: <label id="dados-valor-juno"></label></h4>
          </div>
          <div class="col-md-2"></div>
        </div>
        <div class="row my-5">
          <div class="col"></div>
          <div class="col col-md-6">
            <button class="btn btn-lg  w-100 mx-auto btn-success mx-auto text-center d-block "
                    id="comprar-juno">
              <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
              <span class="d-block"> Efetuar compra</span>
            </button>
          </div>
          <div class="col"></div>
          <div class="col-md-12 text-center my-3 pt-3">
            <img id="trustwaveSealImage"
                 src="https://sealserver.trustwave.com/seal_image.php?customerId=5cb287c9dda14d64b14c29d3257b8c2a&amp;size=105x54&amp;style=invert"
                 border="0" style="cursor:pointer;"
                 onclick="javascript:window.open('https://sealserver.trustwave.com/cert.php?customerId=5cb287c9dda14d64b14c29d3257b8c2a&amp;size=105x54&amp;style=invert', 'c_TW', 'location=no, toolbar=no, resizable=yes, scrollbars=yes, directories=no, status=no, width=615, height=720'); return false;"
                 oncontextmenu="javascript:alert('Copying Prohibited by Law - Trusted Commerce is a Service Mark of TrustWave Holdings, Inc.'); return false;"
                 alt="This site is protected by Trustwave's Trusted Commerce program"
                 title="This site is protected by Trustwave's Trusted Commerce program">
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="info-concluido d-none justify-content-center align-items-center  vh-100">
    <div style="border: 2px solid #000; border-radius: 20px; background: #f0f0f0;" class="p-5">
      <div class="col-sm-12">
        <h2 class="text-uppercase d-flex justify-content-center align-items-center text-center" style="color:#2ecc71">
          <i class="fa fa-check-circle-o fa-2x pr-2" aria-hidden="true" style="color:#27ae60"></i>
          <b>Finalizada com sucesso!</b>
        </h2>
        <h3 class="text-center text-uppercase">Boa sorte!</h3>
        <h4 class="text-center text-uppercase">Certificado(s) enviado(s) por email e no menu "Meus Certificados"</h4>
      </div>
    </div>
  </div>
  <div class="info-rejeitado d-none justify-content-center align-items-center  vh-100">
    <div style="border: 2px solid #000; border-radius: 20px; background: #f0f0f0;" class="p-5">
      <div class="col-sm-12">
        <h2 class="text-uppercase d-flex justify-content-center align-items-center text-center " style="color:red">
          <i class="fa fa-times-circle  fa-2x pr-2" aria-hidden="true" style="color:red"></i>
          <b>Ocorreu um erro!</b>
        </h2>
        <h3 class="text-center text-uppercase">Tente novamente!</h3>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal-ajuda-cvv-juno" tabindex="-1" role="dialog" aria-labelledby="modal-ajuda-cvv-juno"
       aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalScrollableTitle">Ajuda - Onde encontrar o CVV</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <img src="images/what_cvv.jpg" style="max-width: 100%;" alt="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="py-2"></div>
<!--&lt;!&ndash; Ambiente de produção -->
<!--<script src="js/aixmobil-cse-1.1.0.min.js"></script>-->
<!-- Ambiente de teste -->
<!--<script src="https://dev.aixmobil.com.br/resources/cse/js/aixmobil-cse-1.1.0.min.js"></script>-->

<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/main.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/aos.js"></script>
<script src="js/jquery.mask.min.js"></script>
<script src="js/jquery-validation/jquery.validate.js"></script>
<script src="js/jquery-validation/localization/messages_pt_BR.min.js"></script>
<script src="js/loading/dist/jquery.loading.min.js"></script>
<script src="js/sweetalert2/sweetalert2.min.js"></script>
<!-- Validator Plugin to Wizard -->
<script src="js/validator.min.js"></script>
<script src="card/jquery.card.js?v=1.0.21"></script>
<script src="card/card.js?v=1.0.21"></script>
<script src="js/util.js?v=1.0.21"></script>
<!--<script src="js/preenche-cep.js"></script>-->
<script src="js/register.form.js?v=1.0.21"></script>
<!--<script src="js/aixmobil-cse-1.1.0.min.js"></script>-->
<script src="js/aixmobil-cse-dev-1.1.0.min.js"></script>

<script type="text/javascript" src="https://sandbox.boletobancario.com/boletofacil/wro/direct-checkout.min.js"></script>
<!--prod-->
<!--<script type="text/javascript" src="js/direct-checkout.min.js"></script>-->

<script>
    AOS.init();

    function mostrarGateways(gateways) {

        $.each(gateways, function (i, val) {
            $('#gateway-' + val.toLowerCase()).removeClass('d-none');
        });
    }

    function voltarTela() {
        mostraFormularioDoacao();
    }

    function validaCheckboxDoacao() {
        let isChecked = $("#check-doacao").is(":checked");

        if (isChecked !== undefined && isChecked === true) {
            $("#info-content").hide();
        } else {
            $("#info-content").show();
        }
    }

    function mostrarDivCarregando() {
        $('body').loading({
            message: 'Carregando...'
        });
    }

    function esconderDivCarregando() {
        $('body').loading('stop');
    }

    function mostrarDadosDaCompra(dados) {
        dados = dados !== undefined ? dados : undefined;
        if (dados !== undefined && dados.valor !== undefined && dados.valor !== '') {

            let valor = 'R$ ' + parseFloat(dados.valor).toFixed(2).replace('.', ',');
            let nomeCliente = dados.nome;
            let email = dados.email;
            let qtd = parseInt(dados.qtd);

            $('#dadosUsuarioNome').text(nomeCliente);
            $('#dadosUsuarioEmail').text(email);
            $('#dadosUsuarioQtd').text(qtd);
            $('#dadosUsuarioTotal').text(valor);
        }
    }

    // function enviaDadosParaCheckoutERedirecionarParaOPagSeguro(dadosVenda) {
    //     let botaoEnvio = $("#btnFinalizar");
    //     let dados = JSON.parse(dadosVenda);
    //     habilitaDOM(botaoEnvio);
    //     mostraFormularioPagamento();
    // }

    function mostraFormularioEscolherPagamento() {
        $("#conteudo").hide();
        $("#conteudo-selecionar").fadeIn();
    }

    function mostraFormularioPagamentoPagSorte() {
        $("#conteudo").hide();
        $("#conteudo-selecionar").hide();
        $("#conteudo-pagamento").fadeIn();
    }

    function mostraFormularioJuno() {
        $("#conteudo").hide();
        $("#conteudo-selecionar").hide();
        $("#conteudo-pagamento-juno").fadeIn();
    }

    function mostraFormularioPagamento() {
        $("#conteudo").hide();
        $("#conteudo-pagamento").fadeIn();
    }

    function mostraFormularioDoacao() {
        $("#conteudo").fadeIn();
        $("#conteudo-pagamento").hide();
    }

    function fecharAlerta() {
        $('#alerta-error').hide();
    }

    function alertaError(msg) {

        Swal({
            title: 'Atenção',
            text: 'Erro ao Buscar Informações da Venda',
            type: 'warning',
            showCancelButton: false,
            confirmButtonText: 'Ok'
        });
    }

    function alertaErrorPicpay(msg) {

        Swal({
            title: 'Atenção',
            html: msg,
            type: 'warning',
            showCancelButton: false,
            confirmButtonText: 'Ok'
        });
    }

    function mostrarFluxo(gateway) {
        $('#conteudo').hide();
        if (gateway === 'pagsorte') {
            // mostra pagamentos
            $('#conteudo-selecionar').show();
            $('#conteudo-pagamento').hide();
            $("#btnFinalizarEscolher").show();
        } else if (gateway === 'aixmobil') {

            $('#conteudo-selecionar').hide();
            $('#conteudo-pagamento').show();
            $("#btnFinalizarDiretoPagSorte").show();

        } else if (gateway === 'pagseguro') {

            selecionarPagamento('pagseguro');
            // $("#btnFinalizarDiretoPagSeguro").show();

        }
    }

    function selecionarPagamento(gateway) {
        if (gateway === 'pagsorte') {

            mostraFormularioPagamentoPagSorte();
        } else if (gateway === 'pagseguro') {

            checkoutPagseguro();
        } else if (gateway === 'juno') {

            mostraFormularioJuno();
        } else if (gateway === 'picpay') {

            checkoutPicpay();
        }
    }

    function checkoutPagseguro() {


        mostrarDivCarregando();
        let checkbox = $("#check-doacao");
        let isChecked = checkbox.is(":checked");

        const config = {};

        if (!isChecked) {

            let validator = validarFormulario();
            let isValid = validator.valid();
            if (isValid) {
                // $('#form-cadastro')[0]

                $("#form-cadastro").serializeArray().map(function (item) {
                    if (config[item.name]) {
                        if (typeof (config[item.name]) === "string") {
                            config[item.name] = [config[item.name]];
                        }
                        config[item.name].push(item.value);
                    } else {
                        config[item.name] = item.value;
                    }
                });
            }
        }
        var data = {
            transacao: localStorage.getItem('hash'),
            inputs: config
        };

        $.ajaxSetup({
            headers: {
                'AccessToken': localStorage.getItem('dt'),
                'CsrfToken': localStorage.getItem('dtcsrf')
            }
        });
        $.ajax({
            'type': 'POST',
            'url': postCheckoutPagSeguro,
            'contentType': 'application/json',
            'data': JSON.stringify(data),
            'dataType': 'json',
        }).done(function (response) {

            // esconderDivCarregando();
            // setCsrfStorage(response);
            validaRetornoCodigoPagseguro(response);
        }).fail(function () {
            esconderDivCarregando();
            alertaError('Sessão expirada, efetue o login novamente.');
            habilitaDOM($("#btnFinalizarDiretoPagSeguro"));
        });
    }

    function validaRetornoCodigoPagseguro(dados) {

        if (dados !== undefined && dados.url !== undefined && dados.url !== '') {
            // window.open(dados.url, '_blank');
            window.top.location.href = dados.url;
        } else {
            alertaError('Ocorreu um problema ao carregar a tela do PagSeguro! Por favor tente novamente.');
            habilitaDOM($("#btnFinalizarDiretoPagSeguro"));
        }

    }

    function checkoutPicpay() {
        mostrarDivCarregando();
        const url = baseUrlPicpay + 'picpay/post/checkout';
        var data = {
            "transacao": localStorage.getItem('hash'),
            "inputs": {}
        };

        $.ajax({
            'type': 'POST',
            'url': url,
            'contentType': 'application/json',
            'data': JSON.stringify(data),
            'dataType': 'json',
        }).done(function (response) {

            if (response.data.paymenturl) {

                window.top.location.href = response.data.paymenturl;
            } else {
                alertaErrorPicpay('<p>Não foi possivel realizar a compra pelo Picpay, <Br>tente novamente.<p>');
            }
        });
    }

    $(document).ready(() => {

        let form = $("#form-cadastro");
        let dtNascimento = $("#dataNascimento");
        let dtEmissao = $("#dataEmissao");
        let cpf = form.find("input[name='cpf']");
        let cpfBanco = form.find("input[name='cpfTitularConta']");

        dtNascimento.mask("00/00/0000", {
            placeholder: "DD/MM/AAAA",
            clearIfNotMatch: true,
            onComplete: function () {
                var v = dtNascimento.val();
                if (checkWrongDate(v)) {
                    dtNascimento.val("");
                } else {
                }
            }
        });

        dtEmissao.mask("00/00/0000", {
            placeholder: "DD/MM/AAAA",
            clearIfNotMatch: true,
            onComplete: function () {
                var v = dtNascimento.val();
                if (checkWrongDate(v)) {
                    dtNascimento.val("");
                } else {
                }
            }
        });

        $('#data_nascimento').mask('00/00/0000');

        validaCheckboxDoacao();

        cpf.mask("000.000.000-00", {
            placeholder: "123.456.789-00",
            clearIfNotMatch: true
        });

        cpfBanco.mask("000.000.000-00", {
            placeholder: "123.456.789-00",
            clearIfNotMatch: true
        });

        $("#check-doacao").on("click", function (e) {
            let checkbox = $("#check-doacao");
            let isChecked = checkbox.is(":checked");

            if (isChecked !== undefined && isChecked === true) {
                $("#info-content").hide();
            } else {
                $("#info-content").show();
            }
        });


        $("#btnFinalizarEscolher").on('click', function (e) {
            let botaoEnvio = $(this);
            let checkbox = $("#check-doacao");
            let isChecked = checkbox.is(":checked");

            desabilitaDOM(botaoEnvio);

            if (isChecked !== undefined && isChecked === true) {

                // Significa que o usuário está disposto a realizar a doação
                habilitaDOM(botaoEnvio);
                mostraFormularioEscolherPagamento();
            } else {
                let validator = validarFormulario();
                $("#form-cadastro").submit();
                let isValid = validator.valid();
                if (!isValid) {
                    habilitaDOM(botaoEnvio);
                    showInputError("#alerta-error", "Favor preencher todos os dados do formulário.");
                } else {

                    mostraFormularioEscolherPagamento();
                    habilitaDOM(botaoEnvio);
                }
            }
        });

        $("#btnFinalizarDiretoPagSorte").on('click', function (e) {
            let botaoEnvio = $(this);
            let checkbox = $("#check-doacao");
            let isChecked = checkbox.is(":checked");

            desabilitaDOM(botaoEnvio);

            if (isChecked !== undefined && isChecked === true) {

                // Significa que o usuário está disposto a realizar a doação
                habilitaDOM(botaoEnvio);
                mostraFormularioPagamentoPagSorte();
            } else {
                let validator = validarFormulario();
                $("#form-cadastro").submit();
                let isValid = validator.valid();
                if (!isValid) {
                    habilitaDOM(botaoEnvio);
                    showInputError("#alerta-error", "Favor preencher todos os dados do formulário.");
                } else {

                    mostraFormularioPagamentoPagSorte();
                    habilitaDOM(botaoEnvio);
                }
            }
        });

        $("#btnFinalizarDiretoPagSeguro").on('click', function (e) {
            let botaoEnvio = $(this);
            let checkbox = $("#check-doacao");
            let isChecked = checkbox.is(":checked");

            desabilitaDOM(botaoEnvio);

            if (isChecked !== undefined && isChecked === true) {

                // Significa que o usuário está disposto a realizar a doação
                checkoutPagseguro();
            } else {
                let validator = validarFormulario();
                $("#form-cadastro").submit();
                let isValid = validator.valid();
                if (!isValid) {
                    habilitaDOM(botaoEnvio);
                    showInputError("#alerta-error", "Favor preencher todos os dados do formulário.");
                } else {

                    checkoutPagseguro();
                }
            }
        });

        form.submit((e) => {
            e.preventDefault();
        });
        $('form').submit((e) => {
            e.preventDefault();
        });
        $('#form-cadastro-juno').submit((e) => {
            e.preventDefault();
        });
        //     } else {
        //         $("#form-cadastro").remove();
        //         alertaError('Ocorreu um erro inesperado, favor tentar novamente mais tarde ou contate-nos para que possamos ajuda-lo.');
        //         // Não foi possível concluir sua solicitação, por favor tente novamente ou entre em contato conosco pelos canais de comunicação. Desculpe o transtorno.
        //         // $("#conteudo").html("<h4 style='color: red'>Atenção</h4><h5>" + res.data + "</h5>");
        //         $("#conteudo").html("<div style='min-height: 200px;'></div>");
        //         esconderDivCarregando();
        //     }
        //
        // }, fail => {
        //     console.log(fail);
        //     alertaError('Ocorreu um erro inesperado, favor tentar novamente mais tarde ou contate-nos para que possamos ajuda-lo.');
        // });


    });

    function solicitaDadosPagSeguro(dados) {
        return new Promise((resolve, reject) => {
            let request = $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "http://mobile-dev.redepos.com.br/venda/post/checkout",
                // url: "http://192.168.1.181:8089/venda/post/checkout",
                data: JSON.stringify(dados)
            });

            request.done(data => {
                resolve(data);
            });
            request.fail(data => {
                reject(data);
            })
        });
    }

    function buscaInformacoesDataEdicao(idTipoLoteria) {
        return new Promise((resolve, reject) => {
            let request = $.ajax({
                type: "GET",
                contentType: "application/json",
                // url: "http://mobile-dev.redepos.com.br/site/get/datas-sorteio-resgate",
                url: "http://192.168.1.181:8089/site/get/datas-sorteio-resgate",
                data: {idTipoLoteria: idTipoLoteria},
                dataType: "json"

            });

            request.done(data => {
                resolve(data);
            });
            request.fail(data => {
                reject(data);
            })
        });
    }

    function desabilitaDOM(el) {
        el.attr("disabled", true);
    }

    function habilitaDOM(el) {
        el.removeAttr("disabled");
    }

    function buscaInformacoesVenda() {

        return new Promise((resolve, reject) => {
            const urlParams = new URLSearchParams(window.location.search);
            localStorage.hash = urlParams.get('hash');
            $.ajaxSetup({
                headers: {
                    'AccessToken': localStorage.getItem('dt')
                }
            });
            let request = $.ajax({
                type: "GET",
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                // url: baseUrl + "aixmobil/get/ler",
                url: baseUrl + "aixmobil/get/ler",
                data: {
                    hash: localStorage.hash
                },
                dataType: "json"
            });

            request.done(data => {
                resolve(data);
            });
            request.fail(data => {
                reject(data);
            })
        });


    }

    // function enviarFormulario(dadosVenda) {
    //     return new Promise((resolve, reject) => {
    //         var config = {};
    //         $("form").serializeArray().map(function (item) {
    //             if (config[item.name]) {
    //                 if (typeof (config[item.name]) === "string") {
    //                     config[item.name] = [config[item.name]];
    //                 }
    //                 config[item.name].push(item.value);
    //             } else {
    //                 config[item.name] = item.value;
    //             }
    //         });
    //
    //         let request = $.ajax({
    //             type: "POST",
    //             contentType: "application/x-www-form-urlencoded; charset=UTF-8",
    //             // url: "http://mobile-dev.redepos.com.br/site/post/resgate-doacao",
    //             url: "http://192.168.1.181:8089/site/post/resgate-doacao",
    //             data: {inputs: config, dadosVenda},
    //             dataType: "json"
    //         });
    //
    //         request.done(data => {
    //             if (data.error !== undefined) {
    //                 showInputError("#alerta-error", data.error.msg);
    //                 reject(data.error.msg);
    //             } else {
    //                 resolve(data);
    //             }
    //         });
    //
    //         request.fail(data => {
    //             console.log(data);
    //             reject("Não foi possível conectar-se ao servidor. Tente novamente.");
    //         });
    //     });
    //
    // }

    function validarFormulario() {
        return $("#form-cadastro").validate(
            {
                highlight: function (element, errorClass) {
                    $(element.form).find("div.error")
                        .addClass("invalid-feedback");

                    // $(element).addClass("invalid-feedback");
                },
                errorElement: "div",
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent());
                }
            }
        );
    }

    function validarFormularioJuno() {
        return $("#form-cadastro-juno").validate(
            {
                highlight: function (element, errorClass) {
                    $(element.form).find("div.error")
                        .addClass("invalid-feedback");

                    // $(element).addClass("invalid-feedback");
                },
                errorElement: "div",
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent());
                }
            }
        );
    }

    function destruirValidacaoFormulario() {
        let validator = $("form").validate();
        validator.destroy();
    }

    function showInputError(inputId, msg) {
        let input = $(inputId);
        input.find("#alerta-error-msg").html(msg);
        input.show();

    }

    function checkWrongDate(val) {
        var er = /^(((0[1-9]|[12][0-9]|3[01])([-.\/])(0[13578]|10|12)([-.\/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([-.\/])(0[469]|11)([-.\/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([-.\/])(02)([-.\/])(\d{4}))|((29)(\.|-|\/)(02)([-.\/])([02468][048]00))|((29)([-.\/])(02)([-.\/])([13579][26]00))|((29)([-.\/])(02)([-.\/])([0-9][0-9][0][48]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][2468][048]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][13579][26])))$/;

        return !er.test(val);
    }


    // juno
    function nova_compra_juno() {

        // dados do cartão //
        var nomeTitular = $('#juno-nome_titular').val();
        var nome_card = '';
        var primeiro_nome_card = '';
        var sobrenome_card = '';
        if (nomeTitular !== '') {

            nome_card = nomeTitular.split(' ');
            primeiro_nome_card = nome_card[0] !== undefined && nome_card[0] !== '' ? nome_card[0].trim() : '';
            sobrenome_card = nome_card[1] !== undefined && nome_card[1] !== '' ? nome_card[1].trim() : '';
        } else {

            primeiro_nome_card = '';
            sobrenome_card = '';
        }
        //omeTitular.replace(primeiro_nome_card, '').trim();
        var cartao = $("#juno-numero_cartao").val().split(' ').join("");
        var ccv = $("#juno-codigo").val();
        var mesValidade = $('#juno-mes-validade').val();
        var anoValidade = $('#juno-ano-validade').val();
        var dtNascimento = $("#juno-data_nascimento").val().split('/');
        dtNascimento = dtNascimento[2] + "-" + dtNascimento[1] + "-" + dtNascimento[0];

        //dados pessoais
        var email = $("#juno-email").val().trim();
        var telefone = $("#juno-telefone").val().replace(/[^0-9\s]+/g, "").split(" ");
        var cpf = $("#juno-cpf").val().replace(/[^0-9\s]+/g, "");
        var endereco = $("#juno-endereco").val().trim();
        var numero = $("#juno-numero").val();
        var complemento = $("#juno-complemento").val().trim();
        var bairro = $("#juno-bairro").val();
        var cidade = $("#juno-cidade").val();
        var estado = $("#juno-estado").val();
        var pais = $("#juno-pais").val();
        var cep = $("#juno-cep").val().replace(/[^0-9]+/g, "");

        return new Promise((resolve, reject) => {
            getCardHash(primeiro_nome_card, sobrenome_card, cartao, ccv, mesValidade, anoValidade).then(resultado => {
                var json = {
                    "transacao": JSON.stringify(JSON.parse(localStorage.getItem('hash'))),
                    "cardHash": resultado,
                    "charge": {                                                 // INFORMAÇÃO DA COMPRA
                        "description": "Compra de Certificado(s)",
                        "paymentTypes": ['CREDIT_CARD'],
                    },
                    "billing":
                        {
                            "name": primeiro_nome_card + " " + sobrenome_card,  // nome e sobrenome
                            "document": cpf,                                    // cpf do cliente
                            "email": email,                                     // email do cliente
                            "phone": telefone[0],                               // telefone
                            "birthDate": dtNascimento,                          // data de nascimento
                            "notify": false                                     // notificação da juno no email do cliente
                        },
                    "billingAddress": {                                         // ENDERECO DO CLIENTE
                        "street": endereco,
                        "number": numero,
                        "complement": complemento,
                        "district": bairro,
                        "city": cidade,
                        "state": estado,
                        "country": pais,
                        "postalCode": cep
                    }
                };
                resolve(json);
            }, fail => {
                console.log(fail, 'falha Juno index');
                reject(fail);
            });
        });
    }

    // retorna o hash do cartao
    function getCardHash(nome, sobreNome, cartao, ccv, mesValidade, anoValidade) {

        return new Promise((resolve, reject) => {
            /**
             *  Em sandbox utilizar o construtor new DirectCheckout('SEU TOKEN PUBLICO', false);
             * */
            var checkout = new DirectCheckout(junoToken, varAmbienteProducao);

            var cardData = {
                cardNumber: cartao,
                holderName: nome + " " + sobreNome,
                securityCode: ccv,
                expirationMonth: mesValidade,
                expirationYear: anoValidade
            };

            var erro = 'Ocorreu um erro! verifique os dados do cartão e tente novamente!';

            /* isValidSecurityCode: Valida número do cartão de crédito (retorna true se for válido) */
            if (checkout.isValidCardNumber(cardData.cardNumber)) {
                /* isValidExpireDate: Valida data de expiração do cartão de crédito (retorna true se for válido) */
                if (checkout.isValidExpireDate(cardData.expirationMonth, cardData.expirationYear)) {
                    /* isValidSecurityCode: Valida código de segurança do cartão de crédito (retorna true se for válido) */
                    if (checkout.isValidSecurityCode(cardData.cardNumber, cardData.securityCode)) {
                        /* isValidCardData: Validação dos dados do cartão de crédito(retorna true se for válido) */
                        checkout.isValidCardData(cardData, function (validaCartao) {
                            /* getCardType: Obtem o tipo de cartão de crédito (bandeira) */
                            localStorage.flag_card = checkout.getCardType(cardData.cardNumber);
                            checkout.getCardHash(cardData, function (cardHash) {
                                /* Sucesso - A variável cardHash conterá o hash do cartão de crédito */
                                resolve(cardHash);
                            }, function (error) {
                                /* Erro - A variável error conterá o erro ocorrido ao obter o hash */
                                // swetalert('erro','Os Dados do cartão não são validos!');
                                reject('Os Dados do cartão não são validos!');
                            });

                        }, function (error) {
                            /* Erro - A variável error conterá o erro ocorrido durante a validação dos dados do cartão de crédito */
                            reject('Ocorreu um erro! Cartão não pode ser validado! tente novamente!');
                        });
                    } else {
                        reject('Ocorreu um erro! verifique o código de segurança do cartão e tente novamente!');
                    }
                } else {
                    reject('Ocorreu um erro! verifique a data de expiração do cartão e tente novamente!');
                }
            } else {
                reject('Ocorreu um erro! verifique os números do cartão e tente novamente!');
            }
        });
    }


    // mascaras dos campos
    $(".mask-cpf").mask('000.000.000-00');
    $(".mask-telefone").mask('(00) 00000-0000');
    $(".mask-cep").mask('00000-000');
    // $('#numero_cartao').mask('0000.0000.0000.0000');
    $('.mask-codigo').mask('0000');
    $('.mask-validade').mask('00/0000');
    $('.mask-data_nascimento').mask('00/00/0000');
    $('.mask-cvv').mask('000');
    $('.mask-numcartao').mask('0000 0000 0000 0000');

</script>
</body>

</html>
